<?php
require("pubs/virtualphp.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ST ANTHONY CATHOLIC HIGH SCHOOL</title>
<link href="../css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
td{ padding:5px; border:1px #E6E6E6 solid}
td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>
</head>

<body>
<div class="longdiv" style="background:url(../images/bg.png) repeat;"></div>
<div class="container">
	<div id="header" class="hideover">
    	<div class="topheader hideover">
        	<div class="topheaderlogo hideover">
         <div class="schoologo floater hideover"><img src="../uploadedfiles/school_logo/Graphic1.jpg" height="110" width="80" alt="img" align="left" /></div>
                <div class="schooltxt floater">ST ANTHONY CATHOLIC HIGH SCHOOL</div>
            </div>
        </div>
        <div class="menuheader hideover">
        	<ul>
            	<li>
                	<a href=""><div class="hideover" style="background:url(../images/dashboard.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 30px">Home</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/student.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 22px">Students</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/users.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 16px">Employees</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/courses.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 26px">Classes</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/examination.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 13px">Examination</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/attendence.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 13px">Attendance</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/timetable.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 18px">Timetable</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/fees.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 30px">Fees</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/Financial.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 29px">Report</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/settings.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 29px">Settings</div></a>
                </li>
                <li>
                	<div class="hideover"><img src="../images/drop-but.png" alt="img" style="margin-top:30%; margin-left:70%" /></div>
                	
                </li>
            </ul>
        </div>
    </div>
    
    <div id="body" class="hideover">
    	<div class="topbody hideover">Home</div>
        <div class="midbody hideover">
        	<?php require("sidebar.php"); ?>
            
            <div class="midrit floater hideover">
            <div class="text hideover" style="font-size:14pt; padding:0px; color:#5D5D5D; margin-top:4%; font-weight:bold">List of Employees For Scores Entry</div>
            <?php
				if(isset($_GET['bid']) && isset($_GET['cat']))
				{
					$cid = $_GET['bid']; $cat = $_GET['cat'];	
				}
				$sql = "SELECT * FROM employees_subjects,subjects WHERE employees_subjects.subject_id=subjects.id AND batch_id='$cid'";
				$res = ExecuteSQLQuery($sql);
				if($res)
				{
					$num = mysqli_num_rows($res);
					if($num > 0)
					{
						echo '
						<div class="longdiv hideover" style="padding:0px">
						<div class="pullclass hideover" style="margin-top:2%; background:#EEEBE6; cursor:pointer" id="classes">
							<table width="82%" cellspacing="0" cellpadding="0">
            	   	 		<tr>
            	      			<th width="43%" scope="col">Name</th>
            	      			<th width="27%" scope="col">Employee_number</th>
            	      			<th width="30%" scope="col">Score entry</th>
          	      			</tr>
						';
						while($fet=mysqli_fetch_assoc($res))
						{
							$id = $fet['id'];
							$empl = $fet['employee_id'];
							
							$sql2 = "SELECT first_name,last_name,middle_name,employee_number,id FROM employees WHERE id='$empl'";
							$res2 = ExecuteSQLQuery($sql2);
							if($res2)
							{
								$num2 = mysqli_num_rows($res2);
								if($num2 > 0)
								{
									while($fet2=mysqli_fetch_assoc($res2))
									{
										$fname = $fet2['first_name'];
							$lname = $fet2['last_name'];
							$mname = $fet2['middle_name'];
							$no1 = $fet2['employee_number'];
							$nid = $fet2['id'];
							
							echo '
								<tr>
            						<td>'.$lname.' '.$fname.' '.$mname.'</td>
									<td>'.$no1.'</td>
									<td><a href="scoresentry.php?emp='.$nid.'&cid='.$cid.'&cat='.$cat.'"><img src="../images/sd_nav_tick.png" alt="img" /></a></td>
                				</tr>
                	
							';	
											
									}	
								}	
							}
							
						}
						echo '</table></div></div>';
					}	
					else
					{
						
					}
				}
				else
				{
					
				}
			?>
            </div>
        </div>
    </div>
</div>
</body>
</html>