<?php
require("pubs/virtualphp.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ST ANTHONY CATHOLIC HIGH SCHOOL</title>
<link href="../css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
td{ padding:5px; border:1px #E6E6E6 solid}
td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>
</head>

<body>
<div class="longdiv" style="background:url(../images/bg.png) repeat;"></div>
<div class="container">
	<div id="header" class="hideover">
    	<div class="topheader hideover">
        	<div class="topheaderlogo hideover">
         <div class="schoologo floater hideover"><img src="../uploadedfiles/school_logo/Graphic1.jpg" height="110" width="80" alt="img" align="left" /></div>
                <div class="schooltxt floater">ST ANTHONY CATHOLIC HIGH SCHOOL</div>
            </div>
        </div>
        <div class="menuheader hideover">
        	<ul>
            	<li>
                	<a href=""><div class="hideover" style="background:url(../images/dashboard.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 30px">Home</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/student.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 22px">Students</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/users.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 16px">Employees</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/courses.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 26px">Classes</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/examination.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 13px">Examination</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/attendence.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 13px">Attendance</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/timetable.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 18px">Timetable</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/fees.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 30px">Fees</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/Financial.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 29px">Report</div></a>
                </li>
                <li>
                	<a href=""><div class="hideover" style="background:url(../images/settings.png) no-repeat; height:40px; width:90px"></div>
                	<div class="menutext hideover" style="padding:0px 0px 3px 29px">Settings</div></a>
                </li>
                <li>
                	<div class="hideover"><img src="../images/drop-but.png" alt="img" style="margin-top:30%; margin-left:70%" /></div>
                	
                </li>
            </ul>
        </div>
    </div>
    
    <div id="body" class="hideover">
    	<div class="topbody hideover">Home</div>
        <div class="midbody hideover">
        	<?php require("sidebar.php") ?>
            
            <div class="midrit floater hideover">
            <div class="text hideover" style="font-size:14pt; padding:0px; color:#5D5D5D; margin-top:4%; font-weight:bold">Manage Classes & Class Arms</div>
            <?php
				$sql = "SELECT courses.id,course_name FROM courses WHERE is_deleted=0";
				$res = ExecuteSQLQuery($sql);
				if($res)
				{
					$num = mysqli_num_rows($res);
					if($num > 0)
					{
						while($fet=mysqli_fetch_assoc($res))
						{
							$id = $fet['id'];
							$name = $fet['course_name'];
							$sql2 = "SELECT * FROM batches WHERE is_active=1 AND course_id=$id";
							$res2 = ExecuteSQLQuery($sql2);
							if($res2)
							{
								$num2 = mysqli_num_rows($res2);	
							}
							echo '
				<div class="longdiv hideover" style="padding:0px">
            		<div class="pullclass hideover" style="margin-top:2%; background:#EEEBE6; cursor:pointer" id="classes">
                		<span class="text">'.$name.'</span><br />
                    	<span class="text" style="font-size:6pt">'.$num2.' - Class Arm(s)</span>
                	</div>';
								if($num2 > 0)
								{
									echo '
									<div class="pullclass hideover" style="margin-top:1%; padding:0px;">
									<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
                	    				<th width="23%" scope="col">Assign Grade</th>
                	    				<th width="32%" scope="col">Class Teacher</th>
                	    				<th width="12%" scope="col">Enter Score</th>
                	    				<th width="13%" scope="col">End Date</th>
           	        				</tr>';
								while($fet2=mysqli_fetch_assoc($res2))
								{
									$bname=$fet2['name'];
									$lename = substr($bname, 0, 1);
									if($lename == "J"){ $cat="Junior";}
									if($lename == "S"){ $cat="Senior";}
									$bid=$fet2['id'];
									echo '
										
               	  					
                	  				
                	  				<tr>
                	    			<td><a href="grades.php?bid='.$bid.'">'.$bname.'</a></td>
                	    			<td><a href="assign.php?bid='.$bid.'&cat='.$cat.'">Assign Subjects to Teachers</a></td>
                	    			<td><a href="Scores.php?bid='.$bid.'&cat='.$cat.'">Enter score</a></td>
                	    			<td>&nbsp;</td>
           	        				</tr>
                	  
              	  					
									';
								}
									echo '</table>        </div>';
								}
						echo '</div>
							';	
						}	
					}	
					else
					{
						
					}
				}
				else
				{
					
				}
			?>
            </div>
        </div>
    </div>
</div>
</body>
</html>