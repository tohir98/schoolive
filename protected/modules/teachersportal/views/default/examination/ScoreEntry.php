<?php 
session_start();
Yii::app()->clientScript->registerCoreScript('jquery');
require("extends/pubs/virtualphp.php");
?>
<link href="../css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
td{ padding:5px; border:1px #E6E6E6 solid}
td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>
<div id="parent_Sect">
	<?php $this->renderPartial('leftside');?> 
	<div class="right_col"  id="req_res123">
    <!--contentArea starts Here--> 
     <div id="parent_rightSect">
        <div class="parentright_innercon">
            <h1> <?php echo Yii::t('examination','View Examination'); ?></h1>
            <?php //$this->renderPartial('/default/employee_tab');?>
        	<div>
			<?php 
            $employee = Employees::model()->findByAttributes(array('uid'=>Yii::app()->user->id));
			// Get unique batch ID from Timetable. Checking if the employee is teaching.
			$criteria=new CDbCriteria;
			$criteria->select= 'batch_id';
			$criteria->distinct = true;
			$criteria->condition='employee_id=:emp_id';
			$criteria->params=array(':emp_id'=>$employee->id);
			$batches_id = TimetableEntries::model()->findAll($criteria);
			$teach_count = count($batches_id);
			//echo 'Employee ID: '.$employee->id.'<br/>Teaching in '.count($batches_id).' batch(es)<br/>';
			
			//Get unique batch ID from Batches. Checking if the employee is a class teacher.
			$criteria=new CDbCriteria;
			$criteria->select= 'id';
			$criteria->distinct = true;
			$criteria->condition='employee_id=:emp_id';
			$criteria->params=array(':emp_id'=>$employee->id);
			$class_teacher = Batches::model()->findAll($criteria);
			$class_count = count($class_teacher);
			//echo 'Class Teacher of '.count($class_teacher).' batch(es)';
			if($teach_count > 0 or $class_count > 0){
				 $this->renderPartial('examination/exam_tab',array('teach_count'=>$teach_count,'class_count'=>$class_count));
			}
			else{
				?>
                <div class="yellow_bx" style="background-image:none;width:90%;padding-bottom:45px;">
               	<?php
               		echo '
				<table>
					<tr>
                    <td colspan="2"><div id="errorbox2" class="errorbox">'; if(isset($_GET['ret'])){ echo $_GET['ret'];} echo '</div></td>
                  </tr>
				</table>
			';
			if(isset($_GET['emp']) && isset($_GET['subid']) && isset($_GET['cid']))
				{
					$emp = $_GET['emp']; $subid = $_GET['subid']; $cid = $_GET['cid'];	
				}
				$sql4 = "SELECT * FROM batches WHERE id='$cid'";
							$res4 = ExecuteSQLQuery($sql4);
							if($res4)
							{
								$fet4 = mysqli_fetch_assoc($res4);
								$classname = $fet4['name'];	
							}
echo '<div class="text hideover" style="font-size:14pt; padding:0px; color:#5D5D5D; margin-top:4%; font-weight:bold">'.$classname.' List of Subjects Assign for Employee</div>';

				$sql = "SELECT * FROM students WHERE batch_id='$cid' AND is_active=1 AND is_deleted = 0 ORDER BY `first_name` ASC";
				$res = ExecuteSQLQuery($sql);
				if($res)
				{
					$num = mysqli_num_rows($res);
					if($num > 0)
					{
						$co = 1;
						echo '
							<form id="form1" name="form1" method="POST" action="">
							<input type="hidden" name="stud_no" id="stud_no" value="'.$num.'" />
							<input type="hidden" name="subid" value="'.$subid.'" />
							<input type="hidden" name="cid" id="cid" value="'.$cid.'" />
							<input type="hidden" name="emp" id="emp" value="'.$emp.'" />
							<div style="padding:5px; background:#999; color:#FFF">Number of Students in Class is '.$num.'</div>
            	  <table width="82%" cellspacing="0" cellpadding="0">
            	    <tr>
            	      <th width="43%" scope="col">Names</th>
            	      <th width="27%" scope="col">CA</th>
            	      <th width="30%" scope="col">Term Exam</th>
          	      </tr>
						';
						while($fet=mysqli_fetch_assoc($res))
						{
							$ca = ""; 
							$exa = "";
							$name1 = "stuna ".$co;
										
										$name5 = "test ".$co;
										$name6 = "exam ".$co;
										$name3 = "Matno ".$co;
										$sid=$fet['id'];
										$pros = "chk".$co;
							$id = $fet['sub_id'];
							$name = $fet['first_name'].' '.$fet['last_name'].' '.$fet['middle_name'];
						$sql8 = "SELECT * FROM studentscore WHERE cid='$cid' AND emp_id='$emp' AND stuid='$sid' AND sub_id='$subid'";
							$res8 = ExecuteSQLQuery($sql8);
							if($res8)
							{
								$num8 = mysqli_num_rows($res8);
								if($num8 == 1)
								{
									$fet8=mysqli_fetch_assoc($res8);
									$ca = $fet8['ca'];$exa = $fet8['exam'];	
								}	
							}
							echo '
								
            	    <tr>
            	      <td>'.$name.'</td><input type="hidden" name="'.$name1.'" value="'.$name.'" />
					  <input type="hidden" name="'.$name3.'" value="'.$sid.'" />
            	      <td><input name="'.$name5.'" type="text" size="5" value="'.$ca.'" /></td>
            	      <td><input name="'.$name6.'" type="text" size="5" value="'.$exa.'" /></td>
          	      </tr>         	    
							';								
							$co = $co + 1;
						}
						echo '<tr>
            	      <td colspan="1"><input type="submit" name="button" id="button" value="Submit" style="padding:8px" /></td>
					  <input type="hidden" name="MM_insert" value="form1" />
          	      
            	      <td colspan="2"><input type="submit" name="upd" id="upd" value="Update" style="padding:8px" /></td>
					  <input type="hidden" name="MM_update" value="formupd" />
          	      </tr>
          	      </table> </form>';
					}
				}
               	?>
       		</div>
                <?php
			}
			
			// Displaying Default page
			if(Yii::app()->controller->action->id=='examination' and ($teach_count > 0 or $class_count > 0)){ 
				if($teach_count > 0){ // If employee is teaching in any batch, display all batches as list
					$this->renderPartial('examination/allexam',array('employee_id'=>$employee->id));
				}
				elseif($teach_count <= 0 and $class_count > 0){ // If employee is not teaching in any batch, but is a classteacher, display batches in charge.
					$this->renderPartial('examination/classexam',array('employee_id'=>$employee->id));
				}
			}
			
			// Displaying all batches when 'All Class' tab is clicked 
			elseif(Yii::app()->controller->action->id=='allexam' and $_REQUEST['bid'] == NULL){ // Displaying All Batches
				$this->renderPartial('examination/allexam',array('employee_id'=>$employee->id));
			}
			
			// Exam Details
			elseif((Yii::app()->controller->action->id=='allexam' or Yii::app()->controller->action->id=='classexam') and $_REQUEST['bid']!= NULL){ 
				if($_REQUEST['exam_group_id']== NULL){ // Displaying exam lists of a batch
					$this->renderPartial('examination/exams',array('employee_id'=>$employee->id,'batch_id'=>$_REQUEST['bid'])); 
				}
				else{ // Displaying individual exam details of a batch
					if($_REQUEST['r_flag'] == NULL){ // Displaying Schedule
						$this->renderPartial('examination/schedule',array('employee_id'=>$employee->id,'batch_id'=>$_REQUEST['bid'],'exam_group_id'=>$_REQUEST['exam_group_id']));
					}
					elseif($_REQUEST['r_flag'] == 1){ // Displaying Results
						if($_REQUEST['exam_id'] == NULL){
							
							$this->renderPartial('examination/result',array('employee_id'=>$employee->id,'batch_id'=>$_REQUEST['bid'],'exam_group_id'=>$_REQUEST['exam_group_id']));
						}
						else{
							
							$this->renderPartial('examination/examScores/scores',array('employee_id'=>$employee->id,'batch_id'=>$_REQUEST['bid'],'exam_group_id'=>$_REQUEST['exam_group_id'],'exam_id'=>$_REQUEST['exam_id']));
						}
					}
				}
			}

			// Displaying batches in charge when 'My Class' tab is clicked 
			elseif(Yii::app()->controller->action->id=='classexam' and $_REQUEST['r_flag'] == NULL and $_REQUEST['bid'] == NULL){ // Displaying Class Batches
				$this->renderPartial('examination/classexam',array('employee_id'=>$employee->id));
			}
			
			// Displaying score update page
			if(Yii::app()->controller->action->id=='update'){
				$this->renderPartial('examination/examScores/update',array('employee_id'=>$employee->id,'batch_id'=>$_REQUEST['bid'],'exam_group_id'=>$_REQUEST['exam_group_id'],'exam_id'=>$_REQUEST['exam_id']));
			}
			
        	?>
			</div>
		</div>
	</div>
	 <div class="clear"></div>
</div>
