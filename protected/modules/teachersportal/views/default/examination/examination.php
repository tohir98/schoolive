<?php
//echo Yii::app()->user->id;
Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<link href="../css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
    th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
    td{ padding:5px; border:1px #E6E6E6 solid}
    td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>
<div id="parent_Sect">
    <?php $this->renderPartial('leftside'); ?> 
    <div class="right_col"  id="req_res123">
        <!--contentArea starts Here--> 
        <div id="parent_rightSect">
            <div class="parentright_innercon">
                <h1> <?php echo Yii::t('examination', 'View Examination Details'); ?></h1>
                <div>
                    <?php
                    $termId = $current_session['term_id'];
                    $sessionId = $current_session['session_id'];
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
                    }
                    ?>
                    <?php if (!empty($myClasses)) : ?>
                        <h3 style="text-align: center">My Classes</h3>
                        <table align="center" width="70%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr class="pdtab-h">
                                    <td align="center">Class Arm Name</td>
                                    <!--<td align="center">Class Teacher</td>-->
                                    <td align="center">Actions</td>
                                </tr>
                                <?php foreach ($myClasses as $myClass): ?>
                                    <tr id="batchrow13">
                                        <td style="text-align:center; padding-left:10px; font-weight:bold;">
                                            <a href="<?= $this->createUrl('default/viewMyClass&cid='.$myClass['id']) ?>">
                                                <?= $myClass['name'] ?>(<?= $myClass['course_name'] ?>)
                                            </a>
                                        </td>
                                        <td align="center">Report Sheet | 
                                            <a href="index.php?r=teachersportal/default/view_transcript&session_id=<?= $sessionId ?>&term_id=<?=$termId?>&cid=<?= $myClass['id'] ?>" target="_blank" > Broad Sheet </a>
                                            | 
                                            All Report</td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>

                    <div class="text hideover" style="font-size:14px; padding:0px; color:#5D5D5D; margin-top:4%; font-weight:bold; text-align: center">List of Subjects Assign for Employee</div>

                    <?php if (!empty($subjectAssigned)): ?>
                        <table align="center" width="70%" border="0" cellspacing="0" cellpadding="0">
                            <?php
                            foreach ($subjectAssigned as $subject) :
                                ?>

                                <tr>
                                    <td>
                                        <span class="text">
                                            <a href="<?php echo $this->createUrl('default/enterscore&emp=' . $subject['employee_id'] . '&cid=' . $subject['batch_id'] . '&subid=' . $subject['subject_id']); ?>">
                                                <?php echo $subject['name'] ?>
                                            </a>
                                        </span>
                                    </td>
                                </tr>


                            <?php endforeach; ?>
                        </table>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>