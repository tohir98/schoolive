<p>
    &nbsp;
</p>
<table class="" width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="" width="70%"><b>AFFECTIVE & PSYCHOMOTOR (BEHAVIOUR & SKILL)</b></td>
        <td class="" style="text-align: center"><b>AFFECTIVE & PSYCHOMOTOR RATINGS</b></td>
    </tr>
    <tr>
        <td>
            <table class="table">
                <tr>
                    <td>Fluency</td>
                    <td><?=$psychomotor['fluency']?></td>
                    <td>Relationship with Students</td>
                    <td><?=$psychomotor['student_relationship']?></td>
                </tr>
                <tr>
                    <td>Self Control</td>
                    <td><?=$psychomotor['self_control']?></td>
                    <td>Spirit of Co-operation</td>
                    <td><?=$psychomotor['spirit_of_coperation']?></td>
                </tr>
                <tr>
                    <td>Sense of Responsibility</td>
                    <td><?=$psychomotor['sense_of_responsibility']?></td>
                    <td>Attentiveness in Class</td>
                    <td><?=$psychomotor['attentiveness']?></td>
                </tr>
                <tr>
                    <td>Promptness in completing work </td>
                    <td><?=$psychomotor['promptness']?></td>
                    <td>Initiative</td>
                    <td><?=$psychomotor['initiative']?></td>
                </tr>
                <tr>
                    <td>Organizational Ability</td>
                    <td><?=$psychomotor['organizational_ability']?></td>
                    <td>Relationship with Staff</td>
                    <td><?=$psychomotor['staff_relationship']?></td>
                </tr>
                <tr>
                    <td>Honesty</td>
                    <td><?=$psychomotor['honesty']?></td>
                    <td>Handwriting</td>
                    <td><?=$psychomotor['handwriting']?></td>
                </tr>
                <tr>
                    <td>Games, Sports, Gymnastics</td>
                    <td><?=$psychomotor['games']?></td>
                    <td>Musical Skills</td>
                    <td><?=$psychomotor['musical_skill']?></td>
                </tr>
                <tr>
                    <td>Punctuality</td>
                    <td><?=$psychomotor['punctuality']?></td>
                    <td>Attendance at class</td>
                    <td><?=$psychomotor['class_attendance']?></td>
                </tr>
                <tr>
                    <td>Reliability</td>
                    <td><?=$psychomotor['reliability']?></td>
                    <td>Neatness</td>
                    <td><?=$psychomotor['neatness']?></td>
                </tr>
                <tr>
                    <td>Politeness</td>
                    <td><?=$psychomotor['politeness']?></td>
                    <td>Perseverance</td>
                    <td><?=$psychomotor['perseverance']?></td>
                </tr>
            </table>
        </td>
        <td align="right" valign="top">
            <table class="table">
                <tr>
                    <td>5</td>
                    <td>Maintains an excellent degree of Observable trait </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Maintains a high level of observable traits</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Acceptable level of observable traits</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Shows minimal regards for observable traits</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Has no regard for observable traits</td>
                </tr>
            </table>
        </td>
    </tr>
</table>