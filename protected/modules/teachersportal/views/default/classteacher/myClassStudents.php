<?php
//echo Yii::app()->user->id;
Yii::app()->clientScript->registerCoreScript('jquery');
?>
<link href="../css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
    th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
    td{ padding:5px; border:1px #E6E6E6 solid}
    td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>
<div id="parent_Sect">
    <?php $this->renderPartial('leftside'); ?> 
    <div class="right_col"  id="req_res123">
        <!--contentArea starts Here--> 
        <div id="parent_rightSect">
            <div class="parentright_innercon">
                <h1> <?php echo Yii::t('examination', 'View Examination Details'); ?></h1>
                <div>
                    <?php
                    $termId = $current_session['term_id'];
                    $sessionId = $current_session['session_id'];
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
                    }
                    ?>
                    <?php if (!empty($students)) :  $count = 1; ?>
                        <h3 style="text-align: center">Students</h3>
                        <table align="center" width="70%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr class="pdtab-h">
                                    <td align="center">SN</td>
                                    <td align="center">Name</td>
                                    <td align="center">Actions</td>
                                </tr>
                                <?php foreach ($students as $student): ?>
                                    <tr id="batchrow13">
                                        <td><?= $count ?></td>
                                        <td style="text-align:center; padding-left:10px; font-weight:bold;">
                                            <?= $student->first_name ?>
                                            <?= $student->last_name ?>
                                        </td>
                                        <td align="center">
                                            <a href="<?php echo $this->createUrl('default/addPsychomotor&student_id=' . $student['id'].'&cid='.$_GET['cid']); ?>">Psychomotor</a> | 
                                            <a href="<?php echo $this->createUrl('default/reportSheet&student_id=' . $student['id'].'&cid='.$_GET['cid']); ?>" target="_blank" > Report Sheet </a>
                                        </td>
                                    </tr>
                                <?php ++$count; endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>


                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>

