<style>
    table {
        border-collapse: collapse;
        font-size: 10pt;
    }

    .table td, th {
        border: 1px solid #999;
        padding: 0.3rem;
        text-align: left;
    }
    .head{
        background-color: #66CCFF;
    }
</style>
<?php 
$logo = Logo::model()->findAll(); ?>
<div class="atnd_Con" style="padding-left:20px; padding-top:30px; background-image:url('uploadedfiles/school_logo/<?= $logo[0]->photo_file_name ?>'); background-repeat: no-repeat; background-position-x: 50%; background-position-y: 50%; background-size: 30%, auto;">
    <!-- Header -->
    <div style="border-bottom:#666 1px; width:700px; padding-bottom:20px;">
        <table width="" cellspacing="0" cellpadding="0">
            <tr>
                <td class="first">
                    <?php
                    if ($logo != NULL) {
                        echo '<img src="uploadedfiles/school_logo/' . $logo[0]->photo_file_name . '" alt="' . $logo[0]->photo_file_name . '" class="imgbrder" style="height:80px;" />';
                    }
                    ?>
                </td>
                <td align="center" valign="middle" class="first" style="width:300px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="listbxtop_hdng first" style="text-align:left; font-size:22px; width:300px;  padding-left:10px;">
                                <?php $college = Configurations::model()->findAll(); ?>
                                <?php echo $college[0]->config_value; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="listbxtop_hdng first" style="text-align:left; font-size:14px; padding-left:10px;">
                                <?php echo $college[1]->config_value; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="listbxtop_hdng first" style="text-align:left; font-size:14px; padding-left:10px;">
                                <?php echo 'Phone: ' . $college[2]->config_value; ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <!-- End Header -->
    <div style="float: right; margin-right: 20px; margin-top: -40px; border: 2px solid #eee">
        <?php
        if ($model->photo_file_name) :
            file_put_contents($model->photo_file_name, $model->photo_data);
            ?>
        <img src="<?= $model->photo_file_name ?>" style="max-height: 75px" />
        <?php else : ?>
            <img src="uploadedfiles/ppic.jpg" />
        <?php endif; ?>
    </div>
    <table width="40%" cellspacing="0" cellpadding="0">
        <tr>
            <td><strong>Student Name:</strong></td>
            <td><?php echo ucfirst($model->first_name) . ' ' . ucfirst($model->last_name); ?></td>    
        </tr>
        <tr>
            <td><strong>End of Term Report - School Year:</strong></td>
            <td><?= $currentSession['session_name'] . ' ' . $currentSession['term']; ?></td>    
        </tr>
        <tr>
            <?php $ctotal = StudentScore::classTotal($currentSession['session_id'], $currentSession['term_id'], $_GET['cid']) ?>
            <td><strong>Total Scores:</strong></td>
            <td><?= $ctotal['c_total'] ?> (<?= number_format($ctotal['c_ave'], 2) ?>) | Class Position : - | Students in Class: <?= $studentCount ?> </td>    
        </tr>
    </table>

    <table class="table" width="100%" cellspacing="0" cellpadding="0">
        <thead>
            <tr class="head">
                <th>&nbsp;</th>
                <th>CA</th>    
                <th>Term Exam</th>    
                <th>Total</th>    
                <th>C. High</th>    
                <th>C. Low</th>    
                <th>C. Ave</th>    
                <th>Grade</th>    
                <th>Remark</th>    
            </tr>
        </thead>
        <?php if (!empty($results)): ?>
            <tbody>
                <?php
                foreach ($results as $result):
                    $classDist = StudentScore::classHighLow($currentSession['session_id'], $currentSession['term_id'], $_GET['cid'], $result['sub_id']);
                    ?>

                    <tr>
                        <td><?= $result['subject'] ?></td>
                        <td><?= $result['ca'] ?></td>
                        <td><?= $result['exam'] ?></td>
                        <td><?= $result['exam'] + $result['ca'] ?></td>
                        <td><?= $classDist['c_high'] ?></td>
                        <td><?= $classDist['c_min'] ?></td>
                        <td><?= number_format($classDist['c_ave']) ?></td>
                        <td><?= StudentScore::gradeIt($result['exam'] + $result['ca']) ?></td>
                        <td><?= StudentScore::remarkIt($result['exam'] + $result['ca']) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        <?php endif;
        ?>
    </table>

    <?php include '_psychomotor.php'; ?>

    <h3>Grade Keys</h3>
    <table class="table">
        <tr>
            <td>Grades</td>
            <td>F(0-39)</td>
            <td>P(40-49)</td>
            <td>C(50-69)</td>
            <td>A(70-100)</td>
        </tr>
        <tr class="head">
            <td>Remarks</td>
            <td>Fail</td>
            <td>Pass</td>
            <td>Credit</td>
            <td>Distinction</td>
        </tr>
    </table>
</div>