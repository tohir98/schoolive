<title>Academic Transcript</title>
<link rel="icon" type="image/ico" href="/lagooz/uploadedfiles/school_logo/favicon.ico"/>
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<link id="switch_style" href="bootstrap/css/united/bootstrap.css" rel="stylesheet">

<!-- DataTables -->
<link href="bootstrap/css/plugins/datatable/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
<link href="bootstrap/css/plugins/datatable/TableTools.css" rel="stylesheet" type="text/css"/>

<script src="bootstrap/js/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<script src="bootstrap/js/eakroko.js" type="text/javascript"></script>
<script src="bootstrap/js/plugins/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bootstrap/js/plugins/datatable/TableTools.min.js" type="text/javascript"></script>
<script src="bootstrap/js/plugins/datatable/ColReorder.min.js" type="text/javascript"></script>
<script src="bootstrap/js/plugins/datatable/ColVis.js" type="text/javascript"></script>
<script src="bootstrap/js/plugins/datatable/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script src="bootstrap/js/plugins/datatable/dataTables.scroller.min.js" type="text/javascript"></script>



<?php
$session_id = $_GET['session_id'];
$term_id = $_GET['term_id'];
$cid = $_GET['cid'];

$se1 = Session::model()->find("session_id 	=:x", array(':x' => $session_id));
$ter1 = Term::model()->find("term_id 	=:x", array(':x' => $term_id));
$class = Batches::model()->find("id 	=:x", array(':x' => $cid));

$this->breadcrumbs = array(
    Yii::t('transcript', 'Transcript') => array('/transcript'),
    Yii::t('transcript', 'Choose Class Arm') => array("/transcript/chooseClassArm&session_id={$session_id}&term_id={$term_id}"),
);
$studentScore = new StudentScore();
?>
<div class="header">
    <h3>Broad Sheet - <?= $class->name ?> - <?= $se1->session_name ?> - <?= $ter1->term ?> (<?= count($students) ?> Students)</h3>
</div>
<div style="padding: 10px 10px">


    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <?php if (!empty($subjects)) : ?>
                    <th>Name</th>
                    <?php foreach ($subjects as $subject) : ?>
                        <th><?= substr($subject->name, 0, 4) ?></th>
                        <?php
                    endforeach;
                endif;
                ?>
                <th>CCA</th>
                <th>Total</th>
                <th>%</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($students)):
                foreach ($students as $student):
                    $total = 0;
                    $cca = 0;
                    ?>
                    <tr>
                        <td><?= $student->first_name . ' ' . $student->last_name ?></td>
                        <?php
                        foreach ($subjects as $subject) :
                            $score = $studentScore::getTermScore($student->id, $subject->id, $session_id, $term_id, $cid);
                            $ca = $studentScore::getTermCa($student->id, $subject->id, $session_id, $term_id, $cid);
                            $total += $score;
                            $cca += $ca;
                            ?>
                            <td><?= $score; ?></td>
                        <?php endforeach; ?>
                        <td><?= $cca ?></td>
                        <td><?= $total ?></td>
                        <td>%</td>
                    </tr>
                    <?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>

</div>
