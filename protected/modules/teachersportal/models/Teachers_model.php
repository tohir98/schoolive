<?php

/**
 * Description of Teachers_model
 *
 * @author TOHIR
 */
class Teachers_model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getEmployeeId($uid){
        $sql = "Select id from employees where uid = {$uid} ";
        return Yii::app()->db->createCommand($sql)->queryRow()['id'];
    }

    public static function fetchEmployeeSubjects($id_user) {
        $sql = "SELECT es.*, sub.`name`, sub.batch_id "
            . "FROM employees_subjects es "
            . "INNER JOIN subjects sub ON es.subject_id = sub.id "
            . "WHERE employee_id = (SELECT id FROM employees WHERE uid = " . $id_user . " )";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public static function fetchEmployeeClasses($id_user) {
        $emp_id = self::getEmployeeId($id_user);
        $sql1 = "select e.first_name, e.last_name, b.*, c.course_name "
            . "FROM employees e "
            . "INNER JOIN batches b ON e.id = b.employee_id "
            . "INNER JOIN courses c ON b.course_id = c.id "
            . "WHERE b.employee_id = {$emp_id} ";
        return Yii::app()->db->createCommand($sql1)->queryAll();
    }
    
    public static function addPsychomotor($data){
        return Yii::app()->db->createCommand()->insert('psychomotor', $data);
    }
    
    public static function getMyPsycho($student_id, $term_id, $session_id){
        $sql = "SELECT * FROM psychomotor WHERE student_id = {$student_id} AND term_id = {$term_id} AND session_id= {$session_id} ";
        return Yii::app()->db->createCommand($sql)->queryRow();
    }
    
    public static function checkDelete($student_id, $term_id, $session_id){
        if (self::getMyPsycho($student_id, $term_id, $session_id)){
            $sql = "DELETE FROM psychomotor WHERE student_id = {$student_id} AND term_id = {$term_id} AND session_id= {$session_id}";
            return Yii::app()->db->createCommand($sql)->execute();
        }
        return;
    }

}
