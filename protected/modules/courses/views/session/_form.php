<div class="formCon">
    <?php $sessions = $model::model()->findAll(); ?>
    <div class="formConInner">

        <div>

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'session-form',
                'enableAjaxValidation' => false,
            ));
            ?>

            <p class="note"><?php echo Yii::t('session', 'Fields with'); ?><span class="required">*</span><?php echo Yii::t('session', ' are required.'); ?></p>

           
            <table width="60%" border="0" cellspacing="0" cellpadding="0">

                <tr>
                    <td><?php echo $form->labelEx($model, 'session_name'); ?></td>
                    <td><?php echo $form->textField($model, 'session_name', array('size' => 40, 'maxlength' => 255)); ?>
                        <?php echo $form->error($model, 'session_name'); ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><?php echo $form->labelEx($model, 'session_year'); ?></td>
                    <td><?php echo $form->textField($model, 'session_year', array('size' => 40, 'maxlength' => 255)); ?>
                        <?php echo $form->error($model, 'session_year'); ?></td>
                </tr>

            </table>


            <div class="row">

                <?php //echo $form->labelEx($model,'created_at'); ?>
                <?php
                if (Yii::app()->controller->action->id == 'create') {
                    echo $form->hiddenField($model, 'created_at', array('value' => date('Y-m-d')));
                } else {
                    echo $form->hiddenField($model, 'created_at');
                }
                ?>
                <?php echo $form->error($model, 'created_at'); ?>
            </div>


            <!-- Batch Form Ends -->
            <div style="padding:0px 0 0 0px; text-align:left">
                <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('courses', 'Save') : Yii::t('courses', 'Save'), array('class' => 'formbut')); ?>
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div><!-- form -->
</div>

    <div class="pdtab_Con" style="width:97%">
        <div style="font-size:13px; padding:5px 0px"><?php echo Yii::t('students', '<strong>Academic Sessions</strong>'); ?></div>
        <?php if (!empty($sessions)) : ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr class="pdtab-h">
                        <td align="center" height="18">Session</td>
                        <td align="center">Year</td>
                        <td align="center">...</td>
                    </tr>
                </tbody>
                <tbody>
                    <?php foreach ($sessions as $session): ?>
                        <tr>
                            <td align="center"><?php echo $session->session_name ?></td>
                            <td align="center"><?php echo $session->session_year ?></td>
                            <td align="center">Edit | Delete</td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>