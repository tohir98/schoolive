<?php
$this->breadcrumbs=array(
	Yii::t('courses','Classes')=>array('/courses'),
	Yii::t('courses','Sessions'),
);

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">

<h1><?php echo Yii::t('session','Create Academic Session');?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

 	</div>
    </td>
  </tr>
</table>
