<script src="bootstrap/js/jquery.min.js"></script>
<script src="bootstrap/js/angular.min.js"></script>

<?php if ($_SESSION['success'] !== ''): ?>
    <script>alert('<?= $_SESSION['success'] ?>')</script>
    <?php
    $_SESSION['success'] = '';
endif;
?>
<div class="cont_right formWrapper">


    <div class="formCon">
        <?php $subjects = $model::model()->findAll(); ?>
        <?php $classArms = Batches::getActiveBatches(); ?>
        <div class="formConInner" ng-app="subject" ng-controller="subjectCtrl">

            <form id="subject-form" method="post" action="index.php?r=courses/subjects/addSchoolSubjects">

                <p class="note"><?php echo Yii::t('subject', 'Fields with'); ?><span class="required">*</span><?php echo Yii::t('subject', ' are required.'); ?></p>


                <table width="60%" border="0" cellspacing="0" cellpadding="0">

                    <tr ng-repeat="component in data.components">
                        <td>Subject Name</td>
                        <td><input type="text" required name="subjects[{{$index}}][subject_name]" ng-model="component.subject" /> </td>
                        <td> <a href="#" onclick="return false;" ng-click="removeSubject($index)" title="Remove subject">remove</a> </td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><a href="#" onclick="return false;" title="Add new subject" style="font-size: 16px; color: #000; font-weight: bold" ng-click="addSubject()">+</a></td>
                    </tr>

                </table>


                <!-- Batch Form Ends -->
                <div style="padding:0px 0 0 0px; text-align:left">
                    <input type="submit" class="formbut" value="Save" />
                    <?php // echo CHtml::submitButton($model->isNewRecord ? Yii::t('term', 'Save') : Yii::t('subject', 'Save'), array('class' => 'formbut')); ?>
                </div>

            </form>
        </div><!-- form -->
    </div>



    <div class="pdtab_Con">

        <div style="font-size:13px; padding:5px 0px"><?php echo Yii::t('subject', '<strong>School Subjects</strong>'); ?></div>
        <?php if (!empty($subjects)) : ?>
            <form id="frmAssign" method="post" action="index.php?r=courses/subjects/assignToBatch">


                <table width="80%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr class="pdtab-h">
                            <td class='with-checkbox'>&nbsp;</td>
                            <td>Subjects</td>
                            <td align="center">...</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><select name="classArm" id="classArm" required>
                                    <option value="0">Select Class Arm</option>
                                    <?php
                                    if (!empty($classArms)):
                                        foreach ($classArms as $class):
                                            ?>
                                            <option value="<?= $class[id] ?>"><?= $class[name] ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select></td>
                            <td align="center"><button class="formbut" id="assign_batch_btn">Assign</button></td>
                        </tr>

                    </tbody>
                    <tbody>
                        <?php foreach ($subjects as $subject): ?>
                            <tr id=<?= $subject->subject_id;?>>
                                <td class="with-checkbox">
                                    <input type="checkbox" class="_item_checkbox" name="id_subjects[]" value="<?= $subject->subject ?>" /></td>
                                <td align="center"><?= $subject->subject ?></td>
                                <td align="center">Edit | <?php echo CHtml::ajaxLink(Yii::t('subjects', 'Delete'), array('deletes', 'id' => $subject->subject_id), array('success' => 'rowdelete(' . $subject->subject_id . ')'), array('confirm' => 'Do you want to delete this subject ?')); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </form>

        <?php endif; ?>
    </div>

</div>


<script>
    var subApp = angular.module('subject', []);
    subApp.controller('subjectCtrl', function ($scope) {
        $scope.data = {components: []};
        $scope.blankComponent = {id: null, subject: ''};

        $scope.addSubject = function () {
            $scope.data.components.push(angular.copy($scope.blankComponent));
        };

        $scope.removeSubject = function (idx) {
            $scope.data.components.splice(idx, 1);
        };

        $scope.check_all = function () {
            if ($scope.select_all) {
                $scope.select_all = true;
            } else {
                $scope.select_all = false;
            }

            angular.forEach($scope.employees, function (employee) {
                employee.selected = $scope.select_all;
            });
        };

        $scope.addSubject();

    });

    function get_selected_items() {

        var items = [];
        var i = 0;
        $('._item_checkbox').each(function () {
            if ($(this).is(':checked')) {
                items[i] = $(this).attr('data-id_user');
                i++;
            }
        });

        return items;
    }

    $("#assign").click(function () {

        $('#modal-assign').modal('show');
        return false;
    });

    $("#assign_batch_btn").click(function () {
        var items = get_selected_items();

        if (!items.length) {
            alert("Pls select subjects to assign");
            return false;
        }

        if ($("#classArm").val() === '0') {
            alert("Pls select a class arm");
            $("#classArm").focus();
            return false;
        }

        //$('#id_subjects').val(items);

        $("#frmAssign").submit();
    });

    function rowdelete(id)
    {
        $("#" + id).fadeOut("slow");
    }
</script>