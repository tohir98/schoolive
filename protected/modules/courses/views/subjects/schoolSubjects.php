<?php
//$this->breadcrumbs = array(
//    'Subjects' => array('/courses'),
//    'Add Subjects',
//);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">

            <?php $this->renderPartial('//configurations/left_side');?>

        </td>
        <td valign="top">

            <?php $this->renderPartial('_add_subjects', array(
                'model' => $model
            )); ?>
            
        </td>
    </tr>
</table>