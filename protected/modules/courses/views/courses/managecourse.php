<?php
require("extends/pubs/virtualphp.php");
if (isset($_POST['compilebut'])) {
    $clas = $_POST['clas'];
    $term = $_POST['term'];

    $sql1 = "SELECT * FROM subjects WHERE batch_id='$clas'";
    $res1 = ExecuteSQLQuery($sql1);
    if ($res1) {
        $num1 = mysqli_num_rows($res1);
        if ($num1 > 0) {
            while ($fet1 = mysqli_fetch_assoc($res1)) {
                $sub_id = $fet1['id'];
                $sql2 = "SELECT * FROM studentscore WHERE cid='$clas' AND sub_id='$sub_id'";
                $res2 = ExecuteSQLQuery($sql2);

                if ($res2) {
                    $num2 = mysqli_num_rows($res2);
                    if ($num2 > 0) {

                        $scorekip = 0;
                        $stpfl = 1;
                        $su = 0;
                        $sum = 0;
                        $lowscore = 100;
                        while ($fet2 = mysqli_fetch_assoc($res2)) {

                            $ca = $fet2['ca'];
                            $exa = $fet2['exam'];

                            $su = $ca + $exa;
                            // $rp = $rp . 'Rec num: '.$num2.' Sum of Su variable is for '.$stpfl.': '.$su.'<br>';		
                            if ($su > $scorekip) {
                                $scorekip = $su;
                                // $rp = $rp . ' Scorekip variable for '.$stpfl.': '.$su.' '.$scorekip.'<br>';
                            } else {
                                // $rp = $rp . ' Su variable(not greata dan scorekip) is for '.$stpfl.': '.$su . ' '.$scorekip.'<br>';
                            }
                            if ($su < $lowscore) {
                                $lowscore = $su;
                                $rp = $rp . ' Scorekip low variable for ' . $stpfl . ': ' . $su . ' ' . $lowscore . '<br>';
                            }
                            $sum = $sum + $su;

                            if ($stpfl == $num2) {
                                $av = $sum / $num2;
                                $av = number_format($av, 2, '.', '');
                                $sql3 = "SELECT * FROM classhighest WHERE cid='$clas' AND sub_id='$sub_id'";
                                $res3 = ExecuteSQLQuery($sql3);
                                if ($res3) {
                                    $num3 = mysqli_num_rows($res3);
                                    if ($num3 == 1) {
                                        $fet3 = mysqli_fetch_assoc($res3);
                                        $clav = $fet3['clav'];
                                        $clahig = $fet3['clahig'];
                                        $clalow = $fet3['clalow'];
                                        if ($av > $clav) {
                                            $nav = $av;
                                        } else {
                                            $nav = $clav;
                                        }
                                        if ($scorekip > $clahig) {
                                            $nig = $scorekip;
                                        } else {
                                            $nig = $clahig;
                                        }
                                        if ($clalow < $lowscore) {
                                            $low = $clalow;
                                        } else {
                                            $low = $lowscore;
                                        }
                                        $sql4 = "UPDATE classhighest SET clahig='$nig',clav='$nav',clalow='$low' WHERE cid='$clas' AND sub_id='$sub_id'";
                                        $res4 = ExecuteSQLQuery($sql4);
                                        if ($res4) {
                                            $rp = $rp . ' classhighest score update compiled successfully<br>';
                                        } else {
                                            $rp = $rp . " Error in compiling classhighest score<br>";
                                        }
                                    } else {
                                        $sql4 = "INSERT INTO classhighest(cid,sub_id,term,session,clav,clahig, clalow, datecreated) 
										VALUES('$clas','$sub_id','2nd Term','session','$av','$scorekip','$lowscore',NOW())";
                                        $res4 = ExecuteSQLQuery($sql4);
                                        if ($res4) {
                                            $rp = $rp . " classhighest score compiled successfully<br>";
                                        } else {
                                            $rp = $rp . " Error in compiling classhighest score<br>";
                                        }
                                    }
                                } else {
                                    // $rp = $rp . " Classhighest score wouldn't fetch<br>";	
                                }
                            }
                            $stpfl++;
                        }
                    } else {
                        // $rp = $rp . ' No studentscore record is available for this class '.$clas.' '. $sub_id.'<br>';	
                    }
                }
            }
        }
    }
}


if (isset($_POST['compilebut4'])) {
    $clas4 = $_POST['clas4'];
    $term4 = $_POST['term4'];

    $sql5 = "SELECT * FROM students WHERE batch_id='$clas4' AND is_active=1 AND `is_deleted` = 0";
    $res5 = ExecuteSQLQuery($sql5);
    if ($res5) {
        $num5 = mysqli_num_rows($res5);
        if ($num5 > 0) {

            while ($fet5 = mysqli_fetch_assoc($res5)) {
                $rp = $rp . " " . $stuid = $fet5['id'];
                $sql6 = "SELECT * FROM studentscore WHERE cid='$clas4' AND stuid='$stuid'";
                $res6 = ExecuteSQLQuery($sql6);
                if ($res6) {
                    $num6 = mysqli_num_rows($res6);
                    if ($num6 > 0) {
                        $tot = 0;
                        while ($fet6 = mysqli_fetch_assoc($res6)) {
                            $ca1 = $fet6['ca'];
                            $exa1 = $fet6['exam'];
                            $sum1 = $ca1 + $exa1;
                            $tot = $tot + $sum1;
                        }
                        $sql10 = "SELECT total_score FROM student_grades WHERE stuid='$stuid' AND term='2nd Term' AND session='2014/2015'";
                        $res10 = ExecuteSQLQuery($sql10);
                        if ($res10) {
                            $num10 = mysqli_num_rows($res10);
                            if ($num10 == 1) {
                                $sql11 = "UPDATE student_grades SET totat_score='$tot' WHERE stuid='$stuid' AND term='2nd Term' AND session='2014/2015'";
                                $res11 = ExecuteSQLQuery($sql11);
                                if ($res11) {
                                    $rp = $rp . " Student grade position fetch<br>";
                                } else {
                                    $rp = $rp . " He no work Student grade position fetch<br>";
                                }
                            } else {
                                $sql7 = "INSERT INTO student_grades(stuid,total_score,term,session,datemodify) 
										VALUES('$stuid','$tot','2nd Term','2014/2015',NOW())";
                                $res7 = ExecuteSQLQuery($sql7);
                                if ($res7) {
                                    $rp = $rp . " Student grade position fetch<br>";
                                } else {
                                    $rp = $rp . " He no work Student grade position fetch<br>";
                                }
                            }
                        } else {
                            $rp = $rp . " Position score wouldn't fetch<br>";
                        }
                    } else {
                        $rp = $rp . " No student score fetch<br>";
                    }
                }
            }//students loop	
        } else {
            $rp = $rp . ' No student fetch ' . $clas4 . '<br>';
        }
    } else {
        $rp = $rp . " Error in student position<br>";
    }
}
if (isset($_POST['compilebut2'])) {
    $clas = $_POST['clas2'];
    $term = $_POST['term2'];

    header("Location: " . "extends/result.php?cid=$clas&term=$term");
}
if (isset($_POST['print3'])) {
    $stid = $_POST['stid'];
    $term = $_POST['term3'];
    $sess = $_POST['sess'];
    $cid = $_POST['clas4'];

    header("Location: " . "extends/sturesult.php?sid=$stid&term=$term&sess=$sess&cid=$cid");
}

if (isset($_POST['compilebut5'])) {
    $clas6 = $_POST['clas5'];
    $term6 = $_POST['term5'];

    $sql15 = "SELECT * FROM students WHERE batch_id='$clas6' AND is_active=1 AND `is_deleted` = 0";
    $res15 = ExecuteSQLQuery($sql15);
    if ($res15) {
        $num15 = mysqli_num_rows($res15);
        if ($num15 > 0) {
            while ($fet15 = mysqli_fetch_assoc($res15)) {
                $flu = mt_rand(3, 5);
                $rel = mt_rand(3, 5);
                $self = mt_rand(3, 5);
                $spi = mt_rand(3, 5);
                $resp = mt_rand(3, 5);
                $atten = mt_rand(3, 5);
                $prom = mt_rand(3, 5);
                $init = mt_rand(3, 5);
                $org = mt_rand(3, 5);
                $staf = mt_rand(3, 5);
                $hones = mt_rand(3, 5);
                $han = mt_rand(3, 5);
                $gam = mt_rand(3, 5);
                $mus = mt_rand(3, 5);
                $pun = mt_rand(3, 5);
                $dan = mt_rand(3, 5);
                $abil = mt_rand(3, 5);
                $neat = mt_rand(3, 5);
                $poli = mt_rand(3, 5);
                $pers = mt_rand(3, 5);
                $stud = $fet15['id'];
                $sql7 = "INSERT INTO student_behave(Stuid,Term,Session,Fluency,Relationship,Self_Control,Spirit,Responsibility,Attentiveness,Promptnes,Initiative,Organizational,Staff,Honesty,Handwriting,Games,Musical,Punctuality,Attendance,Reliability,Neatness,Politeness,Perseverance,datecreated)
				VALUES('$stud','2nd Term','2014/2015','$flu','$rel','$self','$spi','$resp','$atten','$prom','$init','$org','$staf','$hones','$han','$gam','$mus','$pun','$dan','$abil','$neat','$poli','$pers',NOW())";
                $res7 = ExecuteSQLQuery($sql7);
                if ($res7) {
                    $rp = $rp . " Student behaviour done<br>";
                } else {
                    $rp = $rp . "Error in saving data";
                }
            }
        } else {
            
        }
    }
}
?>
<style>
    #jobDialog123
    {
        height:auto;
    }

    .mcbrow li.col4{ width:95px;}

    .mcbrow li.gtcol1{ width:422px;}
    td{ padding:5px}
    input{ border:1px solid #D1D1D1}
</style>

<?php
$this->breadcrumbs = array(
    $this->module->id,
);
?>
<?php
$posts = Courses::model()->findAll("is_deleted 	=:x", array(':x' => 0));
?>

<?php if ($posts != NULL) {
    ?>
    <script>
        function detail_manage(id)
        {

            var rr = document.getElementById("dropwindemo" + id).style.display;

            if (document.getElementById("dropwindemo" + id).style.display == "block")
            {
                document.getElementById("dropwindemo" + id).style.display = "none";
                $("#openbutton" + id).removeClass('open');
                $("#openbutton" + id).addClass('view');
            }
            else if (document.getElementById("dropwindemo" + id).style.display == "none")
            {
                document.getElementById("dropwindemo" + id).style.display = "block";
                $("#openbutton" + id).removeClass('view');
                $("#openbutton" + id).addClass('open');
            }
        }

        function details(id)
        {

            var rr = document.getElementById("dropwin" + id).style.display;

            if (document.getElementById("dropwin" + id).style.display == "block")
            {
                document.getElementById("dropwin" + id).style.display = "none";
                $("#openbutton" + id).removeClass('open');
                $("#openbutton" + id).addClass('view');
            }
            else if (document.getElementById("dropwin" + id).style.display == "none")
            {
                document.getElementById("dropwin" + id).style.display = "block";
                $("#openbutton" + id).removeClass('view');
                $("#openbutton" + id).addClass('open');
            }
        }
        function rowdelete(id)
        {
            $("#batchrow" + id).fadeOut("slow");
        }
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="247" valign="top">

    <?php $this->renderPartial('left_side'); ?>

            </td>
            <td valign="top">
                <div class="cont_right formWrapper">
                    <h1><?php echo Yii::t('courses', 'Manage Classes & Class Arms'); ?></h1>
                    <div style=" padding:5px; background:#E4E4E4;">
                        <h1><?php echo Yii::t('courses', 'Compile semester result'); ?></h1>
                        <h4><?php if (isset($rp)) {
        echo Yii::t('courses', $rp);
    } ?></h4>
                        <table width="97%" cellspacing="0" cellpadding="0">
                         <!-- <tr>
                            <td width="47%"><form id="form1" name="form1" method="post" action="">
                              <table width="60%" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td width="26%">Classes </td>
                                    <td width="74%"><select name="clas" id="clas">
    <?php
    $sql = "SELECT * FROM batches WHERE is_active=1";
    $res = ExecuteSQLQuery($sql);
    if ($res) {
        $num = mysqli_num_rows($res);
        if ($num > 0) {
            while ($fet = mysqli_fetch_assoc($res)) {
                $name = $fet['name'];
                $nid = $fet['id'];

                echo '
					<option value="' . $nid . '">' . $name . '</option>								
				';
            }
        }
    }
    ?>
                                    </select></td>
                                  </tr>
                                  <tr>
                                    <td>Term </td>
                                    <td><label for="term2"></label>
                                      <label for="term2"></label>
                                      <select name="term" size="1" id="term2">
                                        <option selected="selected">1st Term</option>
                                        <option>2nd Term</option>
                                        <option>3rd Term</option>
                                      </select></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td><input type="submit" name="compilebut" id="compilebut" value="Compile Result" /></td>
                                  </tr>
                                </table>
                            </form></td>
                            <td width="53%"><form id="form13" name="form13" method="post" action="">
                              <table width="60%" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td width="26%">Classes </td>
                                    <td width="74%"><select name="clas3" id="clas3">
    <?php
    $sql = "SELECT * FROM batches WHERE is_active=1";
    $res = ExecuteSQLQuery($sql);
    if ($res) {
        $num = mysqli_num_rows($res);
        if ($num > 0) {
            while ($fet = mysqli_fetch_assoc($res)) {
                $name = $fet['name'];
                $nid = $fet['id'];

                echo '
					<option value="' . $nid . '">' . $name . '</option>								
				';
            }
        }
    }
    ?>
                                    </select></td>
                                  </tr>
                                  <tr>
                                    <td>Term </td>
                                    <td><label for="term3"></label>
                                      <label for="term3"></label>
                                      <select name="term4" size="1" id="term3">
                                        <option selected="selected">1st Term</option>
                                        <option>2nd Term</option>
                                        <option>3rd Term</option>
                                      </select></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td><input type="submit" name="compilebut4" id="compilebut4" value="Compile Student Position" /></td>
                                  </tr>
                                </table>
                            </form></td>
                          </tr> -->
                            <tr>
                                <td valign="top">
                                    <form name="print" id="print" method="post" action="">
                                        <table width="60%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="26%">Classes </td>
                                                <td width="74%"><select name="clas2" id="clas2">
    <?php
    $sql = "SELECT * FROM batches WHERE is_active=1";
    $res = ExecuteSQLQuery($sql);
    if ($res) {
        $num = mysqli_num_rows($res);
        if ($num > 0) {
            while ($fet = mysqli_fetch_assoc($res)) {
                $name = $fet['name'];
                $nid = $fet['id'];

                echo '
					<option value="' . $nid . '">' . $name . '</option>								
				';
            }
        }
    }
    ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td>Term </td>
                                                <td><label for="term5"></label>
                                                    <label for="term5"></label>
                                                    <select name="term2" size="1" id="term5">
                                                        <option selected="selected">1st Term</option>
                                                        <option>2nd Term</option>
                                                        <option>3rd Term</option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td><input type="submit" name="compilebut2" id="compilebut2" value="Print All  Report Sheet" /></td>
                                            </tr>
                                        </table>
                                    </form></td>
                                <td><form name="print3" id="print3" method="post" action="">
                                        <table width="60%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="26%">StudentID</td>
                                                <td width="74%"><label for="stid2"></label>
                                                    <input name="stid" type="text" id="stid2" style="padding:5px;" size="25" /></td>
                                            </tr>
                                            <tr>
                                                <td width="26%">Classes </td>
                                                <td width="74%"><select name="clas4" id="clas4">
    <?php
    $sql = "SELECT * FROM batches WHERE is_active=1";
    $res = ExecuteSQLQuery($sql);
    if ($res) {
        $num = mysqli_num_rows($res);
        if ($num > 0) {
            while ($fet = mysqli_fetch_assoc($res)) {
                $name = $fet['name'];
                $nid = $fet['id'];

                echo '
					<option value="' . $nid . '">' . $name . '</option>								
				';
            }
        }
    }
    ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td>Term</td>
                                                <td><label for="term6"></label>
                                                    <label for="term6"></label>
                                                    <select name="term3" size="1" id="term6">
                                                        <option selected="selected">1st Term</option>
                                                        <option>2nd Term</option>
                                                        <option>3rd Term</option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td>Session</td>
                                                <td><label for="sess2"></label>
                                                    <select name="sess" size="1" id="sess2">
                                                        <option>2014/2015</option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td><input type="submit" name="print3" id="print3" value="Print Student Report Sheet" /></td>
                                            </tr>
                                        </table>
                                    </form></td>
                            </tr>
                            <tr>
                                <td><form id="form9" name="form9" method="post" action="">
                                        <table width="60%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="26%">Classes </td>
                                                <td width="74%"><select name="clas5" id="clas5">
    <?php
    $sql = "SELECT * FROM batches WHERE is_active=1";
    $res = ExecuteSQLQuery($sql);
    if ($res) {
        $num = mysqli_num_rows($res);
        if ($num > 0) {
            while ($fet = mysqli_fetch_assoc($res)) {
                $name = $fet['name'];
                $nid = $fet['id'];

                echo '
					<option value="' . $nid . '">' . $name . '</option>								
				';
            }
        }
    }
    ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td>Term </td>
                                                <td><label for="term7"></label>
                                                    <label for="term7"></label>
                                                    <select name="term5" size="1" id="term7">
                                                        <option selected="selected">1st Term</option>
                                                        <option>2nd Term</option>
                                                        <option>3rd Term</option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td><input type="submit" name="compilebut5" id="compilebut5" value="Compile Student Behave" /></td>
                                            </tr>
                                        </table>
                                    </form></td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>
                        <p style="color:#FFF">&nbsp;</p>


                    </div>

                </div>
                <div id="jobDialog">
                    <div id="jobDialog1">
    <?php
    $posts = Courses::model()->findAll("is_deleted =:x", array(':x' => 0));
    ?>
    <?php $this->renderPartial('_flash'); ?>

                    </div>
                </div>
                <div class="mcb_Con">
                    <!--<div class="mcbrow hd_bg">
                        <ul>
                            <li class="col1">Course Name</li>
                            <li class="col2">Edit</li>
                            <li class="col3">Delete</li>
                            <li class="col4">Add Batch</li>
                            <li class="col5">View Batch</li>
                        </ul>
                     <div class="clear"></div>
                    </div>-->


    <?php foreach ($posts as $posts_1) {
        ?>
                        <div class="mcbrow" id="jobDialog1">
                            <ul>
                                <li class="gtcol1" onclick="details('<?php echo $posts_1->id; ?>');" style="cursor:pointer;">

                                                            <?php echo $posts_1->course_name; ?>
                                                            <?php
                                                            $course = Courses::model()->findByAttributes(array('id' => $posts_1->id, 'is_deleted' => 0));
                                                            $batch = Batches::model()->findAll("course_id=:x AND is_deleted=:y AND is_active=:z", array(':x' => $posts_1->id, ':y' => 0, ':z' => 1));
                                                            ?>
                                    <span><?php echo count($batch) . ' - ' . Yii::t('courses', 'Class Arm(s)'); ?></span>
                                </li>
                                <li class="col2">
                                                            <?php
                                                            echo CHtml::ajaxLink(Yii::t('courses', 'Edit'), $this->createUrl('courses/Edit'), array(
                                                                'onclick' => '$("#jobDialog11").dialog("open"); return false;',
                                                                'update' => '#jobDialog1', 'type' => 'GET', 'data' => array('val1' => $posts_1->id), 'dataType' => 'text'), array('id' => 'showJobDialog123' . $posts_1->id, 'class' => 'edit'));
                                                            ?>
                                </li>
                                <li class="col3"><?php echo CHtml::link(Yii::t('courses', 'Delete'), array('deactivate', 'id' => $posts_1->id), array('confirm' => "Are you sure?\n\n Note: All details (Classes, students, timetable, fees, exam) related to this course will be deleted.", 'class' => 'delete')); ?></li>
                                <li class="col4">
                                                            <?php
                                                            echo CHtml::ajaxLink(Yii::t('courses', 'Add Class Arm'), $this->createUrl('batches/Addnew'), array(
                                                                'onclick' => '$("#jobDialog").dialog("open"); return false;',
                                                                'update' => '#jobDialog', 'type' => 'GET', 'data' => array('val1' => $posts_1->id), 'dataType' => 'text',), array('id' => 'showJobDialog1' . $posts_1->id, 'class' => 'add'));
                                                            ?>
                                </li>
                                <a href="#" id="openbutton<?php echo $posts_1->id; ?>" onclick="details('<?php echo $posts_1->id; ?>');" class="view"><li class="col5"><span class="dwnbg">&nbsp;</span></li></a>
                            </ul>

                            <div class="clear"></div>
                        </div>
                        <!-- Batch Details -->

                        <!--class="cbtablebx"-->

                        <div class="pdtab_Con" id="dropwin<?php echo $posts_1->id; ?>" style="display: none; padding:0px 0px 10px 0px; ">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <!--class="cbtablebx_topbg"  class="sub_act"-->
                                    <tr class="pdtab-h">
                                        <td align="center"><?php echo Yii::t('courses', 'Class Arm Name'); ?></td>
                                        <td align="center"><?php echo Yii::t('courses', 'Class Teacher'); ?></td>
                                        <td align="center"><?php echo Yii::t('courses', 'Start Date'); ?></td>
                                        <td align="center"><?php echo Yii::t('courses', 'End Date'); ?></td>
                                        <td align="center"><?php echo Yii::t('courses', 'Actions'); ?></td>
                                    </tr>
        <?php
        foreach ($batch as $batch_1) {
            echo '<tr id="batchrow' . $batch_1->id . '">';
            echo '<td style="text-align:left; padding-left:10px; font-weight:bold;">' . CHtml::link($batch_1->name, array('batches/batchstudents', 'id' => $batch_1->id)) . '</td>';
            $settings = UserSettings::model()->findByAttributes(array('user_id' => Yii::app()->user->id));
            if ($settings != NULL) {
                $date1 = date($settings->displaydate, strtotime($batch_1->start_date));
                $date2 = date($settings->displaydate, strtotime($batch_1->end_date));
            }
            $teacher = Employees::model()->findByAttributes(array('id' => $batch_1->employee_id));
            echo '<td align="center">';
            if ($teacher) {
                echo $teacher->first_name . ' ' . $teacher->last_name;
            } else {
                echo '-';
            }
            echo '</td>';
            echo '<td align="center">' . $date1 . '</td>';
            echo '<td align="center">' . $date2 . '</td>';
            echo '<td align="center"  class="sub_act">';
            ?> 
            <?php
            echo CHtml::ajaxLink(Yii::t('courses', 'Edit'), $this->createUrl('batches/addupdate'), array(
                'onclick' => '$("#jobDialog123").dialog("open"); return false;',
                'update' => '#jobDialog123', 'type' => 'GET', 'data' => array('val1' => $batch_1->id, 'course_id' => $posts_1->id), 'dataType' => 'text'), array('id' => 'showJobDialog12' . $batch_1->id, 'class' => 'add'));
            echo '' . CHtml::ajaxLink(
                "Delete", $this->createUrl('batches/remove'), array('success' => 'rowdelete(' . $batch_1->id . ')', 'type' => 'GET', 'data' => array('val1' => $batch_1->id), 'dataType' => 'text'), array('confirm' => Yii::t('courses', 'Are you sure?') . '\n\n' . Yii::t('courses', 'Note: All details (students, timetable, fees, exam) related to this Class will be deleted.')));
            ?> <?php
                            echo '  ' . CHtml::link(Yii::t('courses', 'Add Student'), array('/students/students/create', 'bid' => $batch_1->id)) . '</td>';
                            echo '</tr>';
                            echo '<div id="jobDialog123"></div>';
                        }
                        ?>
                                </tbody>
                            </table>
                        </div>
                        <div id='check'></div>
                                <?php }
                                ?>        

                </div>

                </div>
            </td>
        </tr>
    </table>

                                <?php
                            } else {
                                ?>
    <link rel="stylesheet" type="text/css" href="/openschool/css/style.css" />
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="247" valign="top">

    <?php $this->renderPartial('left_side'); ?>

            </td>
            <td valign="top">
                <div style="padding:20px 20px">
                    <div class="yellow_bx" style="background-image:none;width:680px;padding-bottom:45px;">

                        <div class="y_bx_head" style="width:650px;">
    <?php echo Yii::t('courses', 'It appears that this is the first time that you are using this Open-School Setup. For any new installation we recommend that you configure the following:'); ?>
                        </div>
                        <div class="y_bx_list" style="width:650px;">
                            <h1><?php echo CHtml::link(Yii::t('courses', 'Add New Course') . ' &amp; ' . Yii::t('courses', 'Class'), array('courses/create')) ?></h1>
                        </div>

                    </div>

                </div>


            </td>
        </tr>
    </table>

                            <?php } ?>
