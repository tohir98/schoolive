<div class="formCon">

    <div class="formConInner">

        <div>

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'courses-form',
                'enableAjaxValidation' => false,
            ));
            ?>

            <p class="note"><?php echo Yii::t('courses', 'Fields with'); ?><span class="required">*</span><?php echo Yii::t('courses', ' are required.'); ?></p>

            <?php /* ?><?php echo $form->errorSummary($model); ?><?php */ ?>
            <h3>Class</h3>
            <table width="60%" border="0" cellspacing="0" cellpadding="0">

                <tr>
                    <td><?php echo $form->labelEx($model, 'course_name'); ?></td>
                    <td><?php echo $form->textField($model, 'course_name', array('size' => 40, 'maxlength' => 255)); ?>
                        <?php echo $form->error($model, 'course_name'); ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>

            </table>
            <?php
            $daterange = date('Y') + 20;
            $daterange_1 = date('Y') - 30;
            ?>
            <div class="row">
                <?php echo $form->hiddenField($model, 'code', array('value' => '0')); ?>
                <?php echo $form->hiddenField($model, 'section_name', array('value' => '0')); ?>
                <?php //echo $form->labelEx($model,'is_deleted'); ?>
                <?php echo $form->hiddenField($model, 'is_deleted'); ?>
                <?php echo $form->error($model, 'is_deleted'); ?>
            </div>

            <div class="row">

                <?php //echo $form->labelEx($model,'created_at'); ?>
                <?php
                if (Yii::app()->controller->action->id == 'create') {
                    echo $form->hiddenField($model, 'created_at', array('value' => date('Y-m-d')));
                } else {
                    echo $form->hiddenField($model, 'created_at');
                }
                ?>
                <?php echo $form->error($model, 'created_at'); ?>
            </div>

            <div class="row">
                <?php //echo $form->labelEx($model,'updated_at'); ?>
                <?php echo $form->hiddenField($model, 'updated_at', array('value' => date('Y-m-d'))); ?>
                <?php echo $form->error($model, 'updated_at'); ?>
            </div>

            <br />
            <?php
            if (Yii::app()->controller->action->id == 'create') {
                ?>
                <h3>Class Arm</h3>
                <!-- Batch Form -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="15.1%"><?php echo $form->labelEx($model_1, 'name'); ?></td>
                        <td><?php echo $form->textField($model_1, 'name', array('size' => 40, 'maxlength' => 255)); ?>
                            <?php echo $form->error($model_1, 'name'); ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>

                <?php echo $form->hiddenField($model_1, 'start_date'); ?>
                <?php echo $form->hiddenField($model_1, 'end_date'); ?>
                <?php echo $form->hiddenField($model_1, 'is_deleted'); ?>
                <?php echo $form->hiddenField($model_1, 'is_active'); ?>

                <?php
            }
            ?>

            <!-- Batch Form Ends -->
            <div style="padding:0px 0 0 0px; text-align:left">

                <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('courses', 'Save') : Yii::t('courses', 'Save'), array('class' => 'formbut')); ?>
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div><!-- form -->