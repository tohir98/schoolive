<?php
$this->breadcrumbs=array(
	'Classmanagers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Classmanager', 'url'=>array('index')),
	array('label'=>'Create Classmanager', 'url'=>array('create')),
	array('label'=>'Update Classmanager', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Classmanager', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Classmanager', 'url'=>array('admin')),
);
?>

<h1>View Classmanager #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'course_1',
		'course_2',
		'course_3',
		'course_4',
		'course_5',
	),
)); ?>
