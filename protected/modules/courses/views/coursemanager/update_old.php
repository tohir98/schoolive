<?php
$this->breadcrumbs=array(
	'Classmanagers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Classmanager', 'url'=>array('index')),
	array('label'=>'Create Classmanager', 'url'=>array('create')),
	array('label'=>'View Classmanager', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Classmanager', 'url'=>array('admin')),
);
?>

<h1>Update Class manager <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('/courses/manage', array('model'=>$model)); ?>