<?php
$this->breadcrumbs=array(
	'Classmanagers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Classmanager', 'url'=>array('index')),
	array('label'=>'Manage Classmanager', 'url'=>array('admin')),
);
?>

<h1>Create Class manager</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>