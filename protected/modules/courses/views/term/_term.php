<div class="formCon">
    <?php $terms = $model::model()->findAll(); ?>
    <div class="formConInner">

        <div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'session-form',
                'enableAjaxValidation' => false,
            ));
            ?>

            <p class="note"><?php echo Yii::t('session', 'Fields with'); ?><span class="required">*</span><?php echo Yii::t('session', ' are required.'); ?></p>

           
            <table width="60%" border="0" cellspacing="0" cellpadding="0">

                <tr>
                    <td><?php echo $form->labelEx($model, 'term'); ?></td>
                    <td><?php echo $form->textField($model, 'term', array('size' => 20, 'maxlength' => 255)); ?>
                        <?php echo $form->error($model, 'term'); ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><?php echo $form->labelEx($model, 'term_desc'); ?></td>
                    <td><?php echo $form->textField($model, 'term_desc', array('size' => 40, 'maxlength' => 255)); ?>
                </tr>

            </table>


            <!-- Batch Form Ends -->
            <div style="padding:0px 0 0 0px; text-align:left">
                <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('term', 'Save') : Yii::t('term', 'Save'), array('class' => 'formbut')); ?>
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div><!-- form -->
</div>

    <div class="pdtab_Con" style="width:97%">
        <div style="font-size:13px; padding:5px 0px"><?php echo Yii::t('students', '<strong>Academic Terms</strong>'); ?></div>
        <?php if (!empty($terms)) : ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr class="pdtab-h">
                        <td align="center" height="18">Term</td>
                        <td align="center">Description</td>
                        <td align="center">...</td>
                    </tr>
                </tbody>
                <tbody>
                    <?php foreach ($terms as $term): ?>
                        <tr>
                            <td align="center"><?php echo $term->term ?></td>
                            <td align="center"><?php echo $term->term_desc ?></td>
                            <td align="center">Edit | Delete</td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>