<?php

class DefaultController extends RController
{
    
    public $layout = '//layouts/column1';
    
	public function actionIndex()
	{
		$this->render('index');
	}
    
     public function actionview_transcript($cid){
        $score = new StudentScore();
        $subject = new Subjects();
        $this->render('transcript', array(
			'scores' => $score::model()->findAll("cid 	=:x", array(':x' => $cid)),
			'subjects' => $subject::model()->findAll("batch_id 	=:x", array(':x' => $cid)),
        ));
    }
}