<?php

/**
 * Description of view_sessionController
 *
 * @author TOHIR
 */
class TranscriptController extends RController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'rights', // perform access control for CRUD operations
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $count = Yii::app()->db->createCommand('SELECT COUNT(*) FROM studentscore')->queryScalar();
        $courses = new Courses();
        $session = new Session();
        $term = new Term();
        $batches = new Batches();
        $this->render('index', array(
            'sessions' => $session::model()->findAll(),
            'terms' => $term::model()->findAll(),
			'courses' => $courses::model()->findAll(),
			'batches' => $batches::model()->findAll(),
        ));
    }
    
    public function actionview_transcript($session_id, $term_id, $cid){
        $score = new StudentScore();
        $subject = new Subjects();
        $students = new Students();
        
        $this->renderPartial('transcript', array(
			'subjects' => $subject::model()->findAll("batch_id 	=:x", array(':x' => $cid)),
			'students' => $students::model()->findAll("batch_id 	=:x", array(':x' => $cid)),
        ));
    }
    
    public function actionchooseClassArm($session_id, $term_id){
        $transcript = new StudentScore();
        $this->render('class_arm', array(
            'class_arms' => $transcript::fetchClassArm($session_id, $term_id),
            'session_id' => $session_id,
            'term_id' => $term_id,
        ));
    }

}
