<?php
$se1 = Session::model()->find("session_id 	=:x", array(':x' => $session_id));
$ter1 = Term::model()->find("term_id 	=:x", array(':x' => $term_id));

$this->breadcrumbs = array(
    Yii::t('transcript', 'Transcript') => array('/transcript'),
    Yii::t('transcript', $se1->session_name),
    Yii::t('transcript', $ter1->term),
);
//var_dump($class_arms);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <?php $this->renderPartial('left_side'); ?>

        </td>
        <td valign="top">
            <h3><?php echo Yii::t('transcript', 'Choose Class Arm'); ?></h3>
            <div class="pdtab_Con" style="width:97%">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr class="pdtab-h">
                            <td align="center" height="18">Class Arm</td>
                            <td align="center" height="18">Form Teacher</td>
                            <td align="center">...</td>
                        </tr>
                    </tbody>
                    <tbody>
                        <?php
                        foreach ($class_arms as $class):
                            ?>
                            <tr>
                                <td align="center">
                                    <a href="<?php echo $this->createUrl('transcript/view_transcript&session_id=' . $session_id) . '&term_id=' . $term_id . '&cid=' . $class['cid'] ?>" target="_blank">
                                        <?php echo $class['name']; ?>
                                    </a>
                                </td>
                                <td align="center"><?= $class['first_name'] . ' ' . $class['last_name']; ?></td>
                                <td align="center">&nbsp;</td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>
</table>
