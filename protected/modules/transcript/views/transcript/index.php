<?php
$this->breadcrumbs = array(
    Yii::t('transcript', 'Transcript') => array('/transcript'),
//	Yii::t('transcript','View Session'),
);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <?php $this->renderPartial('left_side'); ?>

        </td>
        <td valign="top">
            <div class="cont_right formWrapper">

                <h1><?php echo Yii::t('transcript', 'Choose Session'); ?></h1>

                <?php
                echo $this->renderPartial('_view_session', array(
                    'classes' => $courses,
                    'batches' => $batches,
                    'sessions' => $sessions,
                    'terms' => $terms,
                    )
                );
                ?>
            </div>
        </td>
    </tr>
</table>
