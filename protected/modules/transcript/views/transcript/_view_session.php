<div class="pdtab_Con" style="width:97%">
   
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <tr class="pdtab-h">
                    <td align="center" height="18">Session</td>
                    <td align="center">Term</td>
                </tr>
            </tbody>
            <tbody>
                <?php
                foreach ($sessions as $session):
                    foreach ($terms as $term) :
                            ?>
                            <tr>
                                <td align="center">

                                    <a href="<?php echo $this->createUrl('transcript/chooseClassArm&session_id=' . $session->session_id) .'&term_id='. $term->term_id; ?>">
                <?php echo $session->session_name; ?>
                                    </a>
                                </td>
                                <td align="center"><?php echo $term->term ?></td>
                            </tr>
                        <?php 
                    endforeach;
                endforeach;
                ?>
            </tbody>
        </table>
</div>