
<?php
$this->breadcrumbs = array(
    UserModule::t('Users') => array('/user'),
    UserModule::t('Manage'),
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});	
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('user-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <div id="othleft-sidebar">
                <?php $this->renderPartial('//configurations/left_side'); ?>
            </div>
        </td>
        <td valign="top">
            <div class="cont_right formWrapper">
                <h1><?php echo UserModule::t("Manage Users"); ?> : &nbsp;
                    <?= isset($_GET['type']) ? ucfirst($_GET['type']) : 'Admin(s)' ?> </h1>

                <div class="edit_bttns" style="top:15px; right:20px">
                    <ul>
                        <li>
                            <?php echo CHtml::link('<span>Create Admin</span>', array('/user/admin/create'), array('class' => 'addbttn last')); ?>    </li>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="search-form" style="display:none">
                    <?php
//                    $this->renderPartial('_search', array(
//                        'model' => $model,
//                    ));
                    ?>
                </div><!-- search-form -->
                <div class="emp_tab_nav">
                    <ul style="width:740px;">
                        <li>
                            <?php
                            $active = (!$_GET['type'] || $_GET['type'] === 'admin') ? 'active' : '';
                            echo CHtml::link(Yii::t('Batch', 'Admin'), array('/user/admin/staff&type=admin'), array('class' => $active));
                            ?>
                        </li>
                        <li>
                            <?php
                            $active = $_GET['type'] === 'teacher' ? 'active' : '';
                            echo CHtml::link(Yii::t('Batch', 'Teachers'), array('/user/admin/staff&type=teacher'), array('class' => $active));
                            ?>
                        </li>
                        <li>
                            <?php
                            $active = $_GET['type'] === 'parent' ? 'active' : '';
                            echo CHtml::link(Yii::t('Batch', 'Parents'), array('/user/admin/staff&type=parent'), array('class' => $active));
                            ?>
                        </li>
                        <li>
                            <?php
                            $active = $_GET['type'] === 'student' ? 'active' : '';
                            echo CHtml::link(Yii::t('Batch', 'Students'), array('/user/admin/staff&type=student'), array('class' => $active));
                            ?>
                        </li>
                    </ul>
                </div>
                <div class="grid-view">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="items">
                        <tbody>
                            <tr class="pdtab-h">
                                <td>SN</td>
                                <td>Username</td>
                                <td>Role</td>
                                <td>E-mail</td>
                                <td>Last Visited</td>
                                <td style="width: 54px;">&nbsp;</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <?php
                            if (!empty($model)):
                                $sn = 0;
                                foreach ($model as $m):
                                    ?>
                                    <tr>
                                        <td><?= ++$sn ?></td>
                                        <td><?= $m['username'] ?></td>
                                        <td><?= $m['role'] ?></td>
                                        <td><?= $m['email'] ?></td>
                                        <td><?= $m['lastvisit_at'] ?></td>
                                        <td class="button-column">
                                            <a class="view" title="View" href="index.php?r=user/admin/view&amp;id=<?= $m['id'] ?>"><img src="/assets/308853d9/gridview/view.png" alt="View"></a> 
                                            <a class="update" title="Update" href="index.php?r=user/admin/update&amp;id=<?= $m['id'] ?>"><img src="/assets/308853d9/gridview/update.png" alt="Update"></a> 
                                            <a class="delete" title="Delete" href="index.php?r=user/admin/delete&amp;id=<?= $m['id'] ?>">
                                                <img src="/assets/308853d9/gridview/delete.png" alt="Delete"></a>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>

                        </tbody>
                    </table>
                </div>



            </div>
        </td>
    </tr>
</table>