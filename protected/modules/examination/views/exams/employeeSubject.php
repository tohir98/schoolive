<?php
$emp = Employees::model()->findByPk($_GET['emp_id']);
$this->breadcrumbs = array(
    'Exams' => array('/examination'),
    $batchInfo->name,
    $emp->first_name . ' ' . $emp->last_name,
);
?>
<link href="css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
    th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
    td{ padding:5px; border:1px #E6E6E6 solid}
    td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <?php $this->renderPartial('/default/left_side'); ?>
        </td>
        <td valign="top">
            <?php
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
            }
            ?>
            <div class="yellow_bx" style="background-image:none;width:90%;padding-bottom:45px;">
                <h3>
                    <?= $batchInfo->name ?> List of Subjects Assigned For <?= $emp->first_name . ' ' . $emp->last_name; ?>
                </h3>
                <div class="longdiv hideover" style="padding:0px">
                    <div class="pullclass hideover" style="margin-top:2%; background:#EEEBE6; cursor:pointer" id="classes">
                        <table width="82%" cellspacing="0" cellpadding="0">
                            <tr class="pdtab-h">
                                <td align="center">SN</td>
                                <td align="center">Subject</td>
                                <td align="center">...</td>
                            </tr>
                            <?php
                            $count = 1;
                            if (!empty($subjects)):
                                foreach ($subjects as $s):
                                    ?>
                                    <tr>
                                        <td><?= $count; ?></td>
                                        <td style="text-align: center">

                                            <a href="<?= $this->createUrl('examScore&bid=' . $_GET['bid'] . '&cat=' . $_GET['cat'] . '&emp_id=' . $_GET['emp_id'] . '&sub_id=' . $s['subject_id']); ?>" title="Edit Student Scores"><?= $s['name'] ?></a>
                                        </td>
                                        <td> <a href="<?= $this->createUrl('viewScore&bid=' . $_GET['bid'] . '&cat=' . $_GET['cat'] . '&emp_id=' . $_GET['emp_id'] . '&sub_id=' . $s['subject_id']); ?>" title="View Student Scores">View</a>
                                        </td>

                                    </tr>
                                    <?php
                                    $count++;
                                endforeach;
                            endif;
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>