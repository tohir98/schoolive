<?php
$this->breadcrumbs = array(
    'Exams' => array('/examination'),
    'Manage',
);
?>
<link href="css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
    th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
    td{ padding:5px; border:1px #E6E6E6 solid}
    td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <?php $this->renderPartial('/default/left_side'); ?>
        </td>
        <td valign="top">
            <div class="cont_right">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" >
                        <div style="padding-left:20px;">
                            <div class="clear"></div>
                            <div class="yellow_bx" style="background-image:none;width:90%;padding-bottom:45px;">
                                <div class="text hideover" style="font-size:14pt; padding:0px; color:#5D5D5D; margin-top:4%; font-weight:bold">List of Employees For Scores Entry</div>
                                <div class="longdiv hideover" style="padding:0px">
                                    <div class="pullclass hideover" style="margin-top:2%; background:#EEEBE6; cursor:pointer" id="classes">
                                        <table width="82%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th width="43%" scope="col">Name</th>
                                                <th width="27%" scope="col">Employee_number</th>
                                                <th width="30%" scope="col">Score entry</th>
                                            </tr>
                                            <?php
                                                if(!empty($emps)):
                                                    foreach($emps as $e): ?>
                                            <tr>
                                                <td style="text-align: center"><?= $e['first_name'] . ' ' . $e['last_name'] ?></td>
                                                <td style="text-align: center"><?= $e['employee_number'] ?></td>
                                                <td>
                                                    <a href="<?= $this->createUrl('subjects&bid='.$_GET['bid'].'&cat='.$_GET['cat'] . '&emp_id='.$e['employee_id']) ?>">
                                                        <img src="./images/sd_nav_tick.png" alt="img" />
                                                    </a>
                                                </td>
                                            </tr>
                                                   <?php endforeach;
                                                endif;
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            </div>
            
        </td>
    </tr>
</table>