<?php
$emp = Employees::model()->findByPk($_GET['emp_id']);
$this->breadcrumbs = array(
    'Exams' => array('/examination'),
    $batchInfo->name,
    $emp->first_name . ' ' . $emp->last_name,
    $subject_info->name => array('/examination/exams/subjects&bid=' . $_GET['bid'] . '&cat=' . $_GET['cat'] . '&emp_id=' . $_GET['emp_id']),
    'View Result',
);
?>
<link href="../css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
    th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
    td{ padding:5px; border:1px #E6E6E6 solid}
    td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <?php $this->renderPartial('/default/left_side'); ?>
        </td>
        <td valign="top">
            <div class="pdtab_Con" style="width:97%">
                <a class="formlink" href="<?= $this->createUrl('subjects&bid=' . $_GET['bid'] . '&cat=' . $_GET['cat'] . '&emp_id=' . $_GET['emp_id']) ?>">
                    &Lleftarrow; Back
                </a>
                <br><br><br>
                <h3> <?php echo Yii::t('examination', 'Result: ' . $subject_info->name); ?></h3>

                <div>
                    <div class="yellow_bx" style="background-image:none;width:90%;padding-bottom:45px;">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <th width="43%" scope="col">Names</th>
                                <th width="27%" scope="col">CA</th>
                                <th width="30%" scope="col">Exam</th>
                                <th width="30%" scope="col">Total</th>
                            </tr>
                            <?php
                            $count = 1;
                            if (!empty($students)):
                                foreach ($students as $student):
                                    ?>
                                    <tr>
                                        <td>
                                            <?= $count; ?>.
                                            <?= $student['first_name'] . ' ' . $student['last_name'] ?>
                                        </td>
                                        <td><?= $student['ca'] > 0 ? $student['ca'] : ''; ?></td>
                                        <td><?= $student['exam'] > 0 ? $student['exam'] : ''; ?></td>
                                        <td><?= ($student['ca'] + $student['exam']) > 0 ? $student['ca'] + $student['exam'] : ''; ?></td>
                                    </tr>
                                    <?php
                                    ++$count;
                                endforeach;
                            endif;
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
