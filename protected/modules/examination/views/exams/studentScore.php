<?php
$sc = new StudentScore();
$emp = Employees::model()->findByPk($_GET['emp_id']);
$this->breadcrumbs = array(
    'Exams' => array('/examination'),
    $batchInfo->name,
    $emp->first_name . ' ' . $emp->last_name,
);
?>
<link href="../css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
    th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
    td{ padding:5px; border:1px #E6E6E6 solid}
    td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <?php $this->renderPartial('/default/left_side'); ?>
        </td>
        <td valign="top">
            <div class="pdtab_Con" style="width:97%">
                <a class="formlink" href="<?= $this->createUrl('subjects&bid=' . $_GET['bid'] . '&cat=' . $_GET['cat'] . '&emp_id=' . $_GET['emp_id']) ?>">
                    &Lleftarrow; Back
                </a>
                <br><br><br>

                <h3> <?php echo Yii::t('examination', 'Enter CA/Examination Score: ' . $subject_info->name); ?></h3>

                <div>
                    
                    <?php
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
                    }
                    ?>
                    <div class="yellow_bx" style="background-image:none;width:90%;padding-bottom:45px;">
                        <form method="post" name="frmScore" id="frmScore" action="">

                            <input type="hidden" name="session_id" value="<?= $session_id ?>" />
                            <input type="hidden" name="term_id" value="<?= $term_id ?>" />
                            <table width="82%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th width="43%" scope="col">Names</th>
                                    <th width="27%" scope="col">CA</th>
                                    <th width="30%" scope="col">Term Exam</th>
                                </tr>
                                <?php
                                $count = 1;
                                if (!empty($students)):
                                    foreach ($students as $student):
                                        $ca = '';
                                        $exam = '';
                                        $caExam = $sc::getStudentExamCa($student['id'], $session_id, $term_id, $subject_id, $batch_id);
                                        if ($caExam):
                                            $ca = $caExam['ca'];
                                            $exam = $caExam['exam'];
                                        endif;
                                        ?>
                                        <tr>
                                            <td>
                                                <?= $count; ?>.
                                                <?= $student['first_name'] . ' ' . $student['last_name'] ?>
                                                <input type="hidden" name="score[id][]" value="<?= $student['id'] ?>" />
                                            </td>
                                            <td><input type="text" name="score[ca][]" value="<?= $ca ?>" size="10" /></td>
                                            <td><input type="text" name="score[exam][]" value="<?= $exam ?>" size="10" /></td>
                                        </tr>
                                        <?php
                                        ++$count;
                                    endforeach;
                                endif;
                                ?>
                                <tr>
                                    <td></td>
                                    <td colspan="2">
                                        <input type="submit" name="save" value="Save" class="formbut" />
                                        <input type="hidden" name="subject_id" value="<?= $subject_id ?>" />
                                        <input type="hidden" name="batch_id" value="<?= $batch_id ?>" />
                                        <input type="hidden" name="employee_id" value="<?= $employee_id ?>" />
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
