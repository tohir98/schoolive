<?php

if (!function_exists('gradeIt')) {

    function gradeIt($score) {
        $grade = 'NA';
        if ($score >= 70) {
            $grade = 'A';
        } else if ($score >= 50 && $score < 70) {
            $grade = 'C';
        } else if ($score >= 40 && $score < 50) {
            $grade = 'P';
        } else {
            $grade = 'F';
        }

        return $grade;
    }

}

if (!function_exists('remarkIt')) {

    function remarkIt($score) {
        $remark = 'NA';
        if ($score >= 70) {
            $remark = 'A';
        } else if ($score >= 50 && $score < 70) {
            $remark = 'C';
        } else if ($score >= 40 && $score < 50) {
            $remark = 'P';
        } else {
            $remark = 'F';
        }

        return $remark;
    }

}
