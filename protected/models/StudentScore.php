<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StudentScore
 *
 * @author TOHIR
 */
class StudentScore extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'studentscore';
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'student_id' => Yii::t('student_score', 'Session Name'),
            'session_year' => Yii::t('student_score', 'Session Year'),
            'date_created' => 'Date Created',
        );
    }

    public function getStudentScores(array $params) {
        $sql = "SELECT * "
            . "FROM student_score "
            . "WHERE cid = " . $params['cid'] . " AND sub_id = " . $params['sub_id'] . " AND term_id = " . $params['term_id'] . " AND session_id = " . $params['session_id'] . " AND stuid = " . $params['stuid'];
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function getClassScores(array $params) {
        $sql = "SELECT sc.*, s.first_name, s.last_name "
            . "FROM students s "
            . " LEFT JOIN student_score sc ON sc.stuid = s.id "
            . "WHERE sc.cid = " . $params['cid'] . " AND sc.sub_id = " . $params['sub_id'] . " AND sc.term_id = " . $params['term_id'] . " AND sc.session_id = " . $params['session_id'];
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function viewTranscriptScore() {
        
    }

    public function getTermScore($stuid, $subject_id, $session_id, $term_id, $cid) {
        $sql = "SELECT * "
            . "FROM student_score "
            . "WHERE cid = " . $cid . " AND sub_id = " . $subject_id . " AND term_id = " . $term_id . " AND session_id = " . $session_id . " AND stuid = " . $stuid;
        $score = Yii::app()->db->createCommand($sql)->queryRow();
        return $score ? $score['ca'] + $score['exam'] : 0;
    }

    public function getTermCa($stuid, $subject_id, $session_id, $term_id, $cid) {
        $sql = "SELECT * "
            . "FROM student_score "
            . "WHERE cid = " . $cid . " AND sub_id = " . $subject_id . " AND term_id = " . $term_id . " AND session_id = " . $session_id . " AND stuid = " . $stuid;
        $score = Yii::app()->db->createCommand($sql)->queryRow();
        return $score ? $score['ca'] : 0;
    }

    public function fetchClassArm($session_id, $term_id) {
        $sql = "SELECT DISTINCT s.cid , b.`name`, e.first_name, e.last_name "
            . "FROM student_score s "
            . "INNER JOIN batches b ON s.cid = b.id "
            . "LEFT JOIN employees e ON b.employee_id = e.id "
            . "WHERE s.session_id = {$session_id} AND s.term_id = {$term_id}";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public static function getCurrentSessionTerm() {
        return Yii::app()->db->createCommand("Select css.*, s.session_name, t.term "
                . "from current_session_semester css "
                . "INNER JOIN `session` s ON s.session_id = css.session_id "
                . "INNER JOIN term t ON t.term_id = css.term_id")->queryRow();
    }

    public static function getStudentExamCa($stuid, $session_id, $term_id, $subject_id, $cid) {

        $sql = "SELECT ca, exam "
            . "FROM student_score "
            . "WHERE cid = " . $cid . " "
            . "AND sub_id = " . $subject_id . " "
            . "AND term_id = " . $term_id . " "
            . "AND session_id = " . $session_id . " AND stuid = " . $stuid;
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    public static function getStudentExamScores($stuid, $session_id, $term_id, $cid) {

        $sql = "SELECT sc.* , sub.`name` subject "
            . "FROM student_score sc "
            . "INNER JOIN subjects sub ON sc.sub_id = sub.id "
            . "WHERE sc.stuid = {$stuid} "
            . "AND sc.cid = {$cid} "
            . "AND sc.term_id = {$term_id} "
            . "AND sc.session_id = {$session_id}";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public static function remarkIt($score) {
        $remark = 'NA';
        if ($score >= 70) {
            $remark = 'Distinction';
        } else if ($score >= 50 && $score < 70) {
            $remark = 'Credit';
        } else if ($score >= 40 && $score < 50) {
            $remark = 'Pass';
        } else {
            $remark = 'Fail';
        }

        return $remark;
    }

    public static function gradeIt($score) {
        if ($score >= 70) {
            $grade = 'A';
        } else if ($score >= 50 && $score < 70) {
            $grade = 'C';
        } else if ($score >= 40 && $score < 50) {
            $grade = 'P';
        } else {
            $grade = 'F';
        }

        return $grade;
    }
    
    public static function classHighLow($session_id, $term_id, $cid, $subject_id){
        $sql = "SELECT max(ca + exam) c_high, min(ca + exam) c_min, avg(ca + exam) c_ave "
            . "FROM student_score sc "
            . "INNER JOIN subjects sub ON sc.sub_id = sub.id "
            . "WHERE sc.cid = {$cid} "
            . "AND sc.term_id = {$term_id} "
            . "AND sc.session_id = {$session_id} "
            . "AND sub_id = {$subject_id}";
        return Yii::app()->db->createCommand($sql)->queryRow();
    }
    
    public static function classTotal($session_id, $term_id, $cid){
        $sql = "SELECT sum(ca + exam) c_total, avg(ca + exam) c_ave "
            . "FROM student_score sc "
            . "WHERE sc.cid = {$cid} "
            . "AND sc.term_id = {$term_id} "
            . "AND sc.session_id = {$session_id} " ;
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

}
