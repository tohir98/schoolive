<?php
/**
 * Description of Term
 *
 * @author TOHIR
 */
class Term extends CActiveRecord  {
  /**
	 * Returns the static model of the specified AR class.
	 * @return Courses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'term';
	}
    
    /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('term, term_desc','length', 'max'=>255),
			array('term', 'required'),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('term_id, term', 'safe', 'on'=>'search'),
		);
	}
    
    public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'term' => Yii::t('term','Term'),
			'term_desc' => Yii::t('term','Description'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('term_id',$this->id);
		$criteria->compare('term',$this->term,true);
		$criteria->compare('term_desc',$this->term_desc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}
