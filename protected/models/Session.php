<?php

/**
 * Description of Session
 *
 * @author TOHIR
 */
class Session extends CActiveRecord {
    /**
	 * Returns the static model of the specified AR class.
	 * @return Courses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'session';
	}
    
    /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('session_name, session_year','length', 'max'=>255),
			array('created_at', 'safe'),
			array('session_name, session_year', 'required'),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('session_id, session_name, session_year', 'safe', 'on'=>'search'),
		);
	}
    
    public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'session_name' => Yii::t('session','Session Name'),
			'session_year' => Yii::t('session','Session Year'),
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('session_id',$this->id);
		$criteria->compare('session_name',$this->session_name,true);
		$criteria->compare('session_year',$this->session_year,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}
