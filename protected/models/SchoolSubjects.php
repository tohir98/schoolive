<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SchoolSubjects
 *
 * @author TOHIR
 */
class SchoolSubjects extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Subjects the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'school_subjects';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
//			array('batch_id, no_exams, max_weekly_classes, elective_group_id, is_deleted', 'numerical', 'integerOnly'=>true),
            array('subject', 'length', 'max' => 255),
            array('subject', 'required'),
//			array('subject'),
            array('created_at', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('subject_id, subject, created_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
//	public function relations()
//	{
//		// NOTE: you may need to adjust the relation name and the related
//		// class name for the relations automatically generated below.
//		return array(
//		 'batch123'=>array(self::BELONGS_TO, 'Batches', 'batch_id'),        
//		);
//
//	}


    public function codes($attribute, $params) {

        $flag = 0;
        $subject = Subjects::model()->findAllByAttributes(array('subject' => $this->subject));
        foreach ($subject as $subject_1) {
            if ($subject_1->name == $this->name) {
                $flag = 1;
            }
        }
        if ($flag == 1) {
            $this->addError($attribute, 'This subject is already added');
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'subject' => Yii::t('Subjects', 'Subject Name'),
            'created_at' => 'Created At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('subject_id', $this->subject_id);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function subjectExists($subject, $batch_id) {
        $sql = "SELECT * FROM subjects WHERE `name` = '$subject' AND batch_id = $batch_id ";
        $res = Yii::app()->db->createCommand($sql)->queryAll();
        if (count($res)  > 0) {
            return TRUE;
        }
        return FALSE;
    }

}
