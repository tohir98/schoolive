<?php

/**
 * Description of Exam_model
 *
 * @author TOHIR
 */
class Exam_model {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scoreEntryEmployees($batch_id) {
        $sql = "SELECT DISTINCT es.employee_id, e.first_name, e.last_name, e.employee_number "
            . "FROM employees_subjects es "
            . "INNER JOIN subjects s ON es.subject_id = s.id "
            . "INNER JOIN employees e ON es.employee_id = e.id "
            . "WHERE batch_id={$batch_id} ";

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function scoreEntrySubjects($batch_id, $employee_id) {
        $sql = "SELECT es.employee_id, e.employee_number, s.`name`, es.subject_id "
            . "FROM employees_subjects es "
            . "INNER JOIN subjects s ON es.subject_id = s.id "
            . "INNER JOIN employees e ON es.employee_id = e.id "
            . "WHERE batch_id = {$batch_id}  AND es.employee_id = {$employee_id} ";

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function processStudentScore($params) {
        $studentScore = new StudentScore();
        $data = $params['score'];
        for ($i = 0; $i < count($data['id']); $i++) {
            $where = array(
                'stuid' => $data['id'][$i],
                'sub_id' => $params['subject_id'],
                'cid' => $params['batch_id'],
                'session_id' => $params['session_id'],
                'term_id' => $params['term_id'],
            );

            if (!$studentScore::getStudentScores($where)) {
                $data_db = array_merge($where, array(
                    'emp_id' => $params['employee_id'],
                    'ca' => $data['ca'][$i],
                    'exam' => $data['exam'][$i],
                    'date_created' => date('Y-m-d h:i:s')
                ));
                Yii::app()->db->createCommand()->insert('student_score', $data_db)->execute;
            }else{
                $updateSql = "UPDATE student_score SET ca = ".$data['ca'][$i].", exam = ".$data['exam'][$i]." WHERE stuid = ".$data['id'][$i]." AND sub_id = ".$params['subject_id']." AND cid = ".$params['batch_id']." AND session_id = ".$params['session_id']." AND term_id = ".$params['term_id'] ;
                Yii::app()->db->createCommand($updateSql)->execute();
            }
        }
        return true;
    }

}
