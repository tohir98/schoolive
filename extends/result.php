<?php
error_reporting(0);
session_start();
require("pubs/virtualphp.php");

//var_dump(getLogo()); 
$actual_link = "http://$_SERVER[HTTP_HOST]"; 

if(isset($_GET['cid']) && isset($_GET['term']))
{
	$cid = $_GET['cid'];
	$term = $_GET['term'];	
}
else
{
	exit();	
}
include("PDF_API.php");

header("Expire: Mon, 26 jul 1997 05:05:00 GMT");
header("Last-Modified: ".gmdate('D,d M Y H:i:s')." GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0",false);
header("pragma: no-cache");

$pdf=new FPDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->SetFont('Times','B',16);
$pdf->SetLeftMargin(20);
$pdf->SetDisplayMode(real,'single');



$sql7 = "SELECT name FROM batches WHERE id='$cid'";
$res7 = ExecuteSQLQuery($sql7);
if($res7)
{
	$fet7 = mysqli_fetch_assoc($res7);
	$claname = $fet7['name'];
	$lename = substr($claname, 0, 1);
	if($lename == "J"){ $cat="Junior"; $col1="F(0-39)";$col2="P(40-49)";$col3="C(50-69)";$col4="A(70-100)";}
	if($lename == "S"){ $cat="Senior";$col1="F9(0-39)";$col2="E8(40-44)";$col3="D7(45-49)";$col4="C6(50-54)";$col5="C5(55-59)";$col6="C4(60-64)";$col7="B3(65-69)";$col8="B2(70-74)";$col9="A1(75-100)";}
}

$sql14="SELECT * FROM subjects WHERE batch_id='$cid'";
$res14 = ExecuteSQLQuery($sql14);
if($res14)
{
	$num14=mysqli_num_rows($res14);
}
$sql2 = "SELECT * FROM students WHERE batch_id='$cid' AND is_active=1 AND `is_deleted` = 0";
$res2 = ExecuteSQLQuery($sql2);
if($res2)
{
	$num2 = mysqli_num_rows($res2);
	if($num2 > 0)
	{
		while($fet2=mysqli_fetch_assoc($res2))
		{
		
			$pdf->AddPage();
	
			getLogo() ? $pdf->Image($actual_link.'/lagooz/uploadedfiles/school_logo/'.getLogo(),34,40,150) : $pdf->Image($actual_link.'/uploadedfiles/school_logo/default-logo.jpg',34,40,150);
			//$pdf->Image('../uploadedfiles/school_logo/'.getLogo(),34,40,150);        //enable to show school logo
			
			$pdf->SetY($pdf->GetY()+10);
	
 			$pdf->SetFont('Times','B',14);
			$pdf->SetX($pdf->GetX()+20);
    		$pdf->Cell(40,5,'LAGOOZ SCHOOL, LAGOS.',0,1);
			
			$pdf->SetY($pdf->GetY()+10);	
			$pdf->SetY($pdf->GetY()+8);
			$id = $fet2['id'];

					$name = $fet2['last_name'].' '.$fet2['first_name'].' '.$fet2['middle_name'];	
					$stuid = $fet2['id'];
			//$pdf->Image('../uploadedfiles/school_logo/'.  getLogo(),6,6,25);    //        enable to show school logo
			getLogo() ? $pdf->Image($actual_link.'/lagooz/uploadedfiles/school_logo/'.getLogo(),6,6,25) : $pdf->Image($actual_link.'/uploadedfiles/school_logo/default-logo.jpg',6,6,25);
if ($fet2['photo_file_name']){
			file_put_contents($fet2['photo_file_name'],$fet2['photo_data']);
			$pdf->Image($fet2['photo_file_name'],160,35,25);
			}else{
			$pdf->Image('img/ppic.jpg',160,35,25);
			}
			$pdf->SetFont('Times','B',8);
	$pdf->SetX($pdf->GetX()+-10);
    $pdf->Cell(30,5,'STUDENT NAME:',0,1);
	$pdf->SetY($pdf->GetY()+-5);
	
	$pdf->SetFont('Times','B',10);
	$pdf->SetX($pdf->GetX()+14);
    $pdf->Cell(40,5,strtoupper($name),0,1,'L');
	$pdf->SetY($pdf->GetY()+-5);
	
	$pdf->SetFont('Times','B',8);
	$pdf->SetX($pdf->GetX()+80);
    $pdf->Cell(30,5,'CLASS:',0,1);
	$pdf->SetY($pdf->GetY()+-5);
	
	$pdf->SetFont('Times','B',8);
	$pdf->SetX($pdf->GetX()+90);
    $pdf->Cell(40,5,strtoupper($claname),0,1,'L');
	$pdf->SetY($pdf->GetY()+ 2);
	
	
	$pdf->SetFont('Times','B',8);
	$pdf->SetX($pdf->GetX()+-10);
    $pdf->Cell(30,5,'END OF TERM REPORT - SCHOOL YEAR -:',0,1);
	$pdf->SetY($pdf->GetY()+-5);
	
	$pdf->SetFont('Times','B',8);
	$pdf->SetX($pdf->GetX()+47);
    $pdf->Cell(40,5,"2014/2015 2nd Term",0,1,'L');
	
	$pdf->SetY($pdf->GetY()+ 2);
	
	$sql12 = "SELECT * FROM student_grades WHERE stuid='$stuid' AND cid='$cid'";
	$res12 = ExecuteSQLQuery($sql12);
	if($res12)
	{
		$fet12=mysqli_fetch_assoc($res12);
		$scorepos=$fet12['total_score'];
	}
	
	$sql8 = "SELECT * FROM student_grades WHERE cid='$cid' ORDER BY `total_score` ASC";
	$res8 = ExecuteSQLQuery($sql8);
	if($res8)
	{
		$num8 = mysqli_num_rows($res8);
		if($num8 > 0)
		{
			$sn2 = 1; $cou=0;$cou1=0;
			while($fet8=mysqli_fetch_assoc($res8))
			{
				$totalscor=$fet8['total_score'];
				$stuid2=$fet8['stuid'];
				
				if($scorepos > $totalscor)
				{
					$cou++;
				}
				else
				{
					$cou1++;
				}
			
				$sn2++;
			}
			
		}	
	}
	$resfor=number_format($scorepos/$num14,2);
	$pdf->SetFont('Times','B',8);
	$pdf->SetX($pdf->GetX()+-10);
    $pdf->Cell(30,5,'Total Scores -:',0,1);
	$pdf->SetY($pdf->GetY()+-5);
	
	$pdf->SetFont('Times','B',8);
	$pdf->SetX($pdf->GetX()+10);
    $pdf->Cell(75,5,$scorepos.' ('.$resfor.'%)'. ' | Class Position: '.$cou1.' | Students in Class: '.$num2,0,1,'L');
	$pdf->SetY($pdf->GetY()+4);
	
			
			$pdf->SetLineWidth(0.1);
			$pdf->SetTextColor(0,0,0);
			
			$sql3 = "SELECT * FROM studentscore WHERE cid='$cid' AND stuid='$stuid'";
			$res3 = ExecuteSQLQuery($sql3);
			if($res3)
			{
				$num3 = mysqli_num_rows($res3);
				if($res3 > 0)
				{
					$pdf->SetX($pdf->GetX()+-10);
					$pdf->SetFillColor(154,220,255);
	 				$pdf->SetFont('Times','',9);
					
  					
					$pdf->Cell(40,6,"       ",1,0,'C',1);
					$pdf->Cell(15,6,"CA",1,0,'C',1);
					$pdf->Cell(28,6,"Term Exam",1,0,'C',1);
					$pdf->Cell(17,6,"Total",1,0,'C',1);
					$pdf->Cell(17,6,"C.High",1,0,'C',1);
					$pdf->Cell(17,6,"C.Low",1,0,'C',1);
					$pdf->Cell(17,6,"C.Ave",1,0,'C',1);
					$pdf->Cell(17,6,"Grade",1,0,'C',1);
					$pdf->Cell(17,6,"Remark",1,0,'C',1);
					
					$pdf->SetY($pdf->GetY()+6);
					$sn = 1;
					while($fet3=mysqli_fetch_assoc($res3))
					{
						if($sn % 2 == 0)
						{
							$pdf->SetFillColor(204,236,255);	
						}
						else
						{
							$pdf->SetFillColor(255,255,255);
						}
						$pdf->SetX($pdf->GetX()+-10);
						$subid=$fet3['sub_id'];
						$ca=$fet3['ca'];
						$exa=$fet3['exam']; $to2 = $ca + $exa;
						$sql6 = "SELECT * FROM grades WHERE class='$cat'";
								$res6 = ExecuteSQLQuery($sql6);
								if($res6)
								{
									$num6 = mysqli_num_rows($res6);
									if($num6 > 0)
									{
										while($fet6=mysqli_fetch_assoc($res6))
										{
											$mrk1 = $fet6['mrk1']; $mrk2 = $fet6['mrk2'];
											if(($to2 >= $mrk1) && ($to2 <= $mrk2))
											{
												$remk = $fet6['remarks']; $grads = $fet6['grades'];	
											}	
										}
										
									}	
								}
						$sql4 = "SELECT * FROM subjects WHERE id='$subid'";
						$res4 = ExecuteSQLQuery($sql4);
						if($res4)
						{
							$num4 = mysqli_num_rows($res4);
							if($num4 == 1)
							{
								$fet4=mysqli_fetch_assoc($res4);
								$subname = $fet4['name'];
								$tota = $ca + $exa;
							}
						}
						$sql5 = "SELECT * FROM classhighest WHERE cid='$cid' AND sub_id='$subid'";
						$res5 = ExecuteSQLQuery($sql5);
						if($res5)
						{
							$num5 = mysqli_num_rows($res5);
							if($num5 == 1)
							{
								$fet5=mysqli_fetch_assoc($res5);
								$clav = $fet5['clav']; $clahigh = $fet5['clahig'];  $clow = $fet5['clalow'];
								
							}	
						}
								
								$pdf->Cell(40,6,$subname,1,0,'C',1);
								$pdf->Cell(15,6,$ca,1,0,'C',1);
								$pdf->Cell(28,6,$exa,1,0,'C',1);
								$pdf->Cell(17,6,$tota,1,0,'C',1);
								$pdf->Cell(17,6,$clahigh,1,0,'C',1);
								$pdf->Cell(17,6,$clow,1,0,'C',1);
								$pdf->Cell(17,6,$clav,1,0,'C',1);
								$pdf->Cell(17,6,$grads,1,0,'C',1);
								$pdf->Cell(17,6,$remk,1,0,'C',1);
								
								$pdf->SetY($pdf->GetY()+6);
								
						$sn++;
					}	
					
				}	
			}
			
					
					$pdf->SetDrawColor(0,0,0);
	 				$pdf->SetFillColor(0,0,0);
	 				$pdf->SetFont('Times','',7);
					$pdf->SetLineWidth(0.1);
  					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-10);
					$pdf->SetFont('Times','B',11);
    				$pdf->Cell(40,5,'AFFECTIVE & PSYCHOMOTOR (BEHAVIOUR & SKILL)',0,1);
					$pdf->SetFont('Times','',7);
					$pdf->SetX($pdf->GetX()+-10);
					$sql8 = "SELECT * FROM student_behave WHERE stuid='$stuid' AND Term='2nd Term' AND Session='2014/2015'";
					$res8 = ExecuteSQLQuery($sql8);
					if($res8)
					{
						$num8 = mysqli_num_rows($res8);
						if($num8 == 1)
						{
							$fet8=mysqli_fetch_assoc($res8);
				$flu = $fet8['Fluency']; $rel = $fet8['Relationship']; $self = $fet8['Self_Control']; $spi = $fet8['Spirit']; 
				$resp = $fet8['Responsibility']; $atten= $fet8['Attentiveness'];$prom = $fet8['Promptnes']; $init = $fet8['Initiative']; 
				$org = $fet8['Organizational']; $staf = $fet8['Staff']; $hones = $fet8['Honesty'];$han = $fet8['Handwriting']; 
				$gam = $fet8['Games']; $mus = $fet8['Musical']; $pun = $fet8['Punctuality'];  	$dan = $fet8['Attendance']; 
				$abil = $fet8['Reliability'];$neat = $fet8['Neatness']; $poli = $fet8['Politeness']; $pers = $fet8['Perseverance'];
						}	
					}
							
										
					$pdf->Cell(40,6,"Fluency",1,0,'L');
					$pdf->Cell(15,6,$flu,1,0,'L');
					$pdf->Cell(35,6,"Relationship with Students",1,0,'L');
					$pdf->Cell(15,6,$rel,1,0,'L');
					
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-10);

					$pdf->SetFillColor(204,236,255);
					$pdf->Cell(40,6,"Self Control",1,0,'L',1);
					$pdf->Cell(15,6,$self,1,0,'L',1);
					$pdf->Cell(35,6,"Spirit of Co-operation",1,0,'L',1);
					$pdf->Cell(15,6,$spi,1,0,'L',1);
					
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-10);

					$pdf->Cell(40,6,"Sense of Responsibility",1,0,'L');
					$pdf->Cell(15,6,$resp,1,0,'L');
					$pdf->Cell(35,6,"Attentiveness in Class",1,0,'L');
					$pdf->Cell(15,6,$atten,1,0,'L');
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-10);

					$pdf->SetFillColor(204,236,255);
					$pdf->Cell(40,6,"Promptness in completing work",1,0,'L',1);
					$pdf->Cell(15,6,$prom,1,0,'L',1);
					$pdf->Cell(35,6,"Initiative",1,0,'L',1);
					$pdf->Cell(15,6,$init,1,0,'L',1);
					
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-10);

					$pdf->Cell(40,6,"Organizational Ability",1,0,'L');
					$pdf->Cell(15,6,$org,1,0,'L');
					$pdf->Cell(35,6,"Relationship with Staff",1,0,'L');
					$pdf->Cell(15,6,$staf,1,0,'L');
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-10);

					$pdf->SetFillColor(204,236,255);
					$pdf->Cell(40,6,"Honesty",1,0,'L',1);
					$pdf->Cell(15,6,$hones,1,0,'L',1);
					$pdf->Cell(35,6,"Handwriting",1,0,'L',1);
					$pdf->Cell(15,6,$han,1,0,'L',1);
					
				
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-10);

					$pdf->Cell(40,6,"Games, Sports, Gymnastics",1,0,'L');
					$pdf->Cell(15,6,$gam,1,0,'L');
					$pdf->Cell(35,6,"Musical Skills",1,0,'L');
					$pdf->Cell(15,6,$mus,1,0,'L');
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-10);	

					$pdf->SetFillColor(204,236,255);
					$pdf->Cell(40,6,"Punctuality",1,0,'L',1);
					$pdf->Cell(15,6,$pun,1,0,'L',1);
					$pdf->Cell(35,6,"Attendance at class",1,0,'L',1);
					$pdf->Cell(15,6,$dan,1,0,'L',1);
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-10);

					$pdf->Cell(40,6,"Reliability",1,0,'L');
					$pdf->Cell(15,6,$abil,1,0,'L');
					$pdf->Cell(35,6,"Neatness",1,0,'L');
					$pdf->Cell(15,6,$neat,1,0,'L');
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-10);

					$pdf->SetFillColor(204,236,255);
					$pdf->Cell(40,6,"Politeness",1,0,'L',1);
					$pdf->Cell(15,6,$poli,1,0,'L',1);
					$pdf->Cell(35,6,"Perseverance",1,0,'L',1);
					$pdf->Cell(15,6,$pers,1,0,'L',1);
					
					
					$pdf->SetY($pdf->GetY()+10);
					$pdf->SetX($pdf->GetX()+-10);
					$pdf->SetFont('Times','B',10);
    				$pdf->Cell(40,5,'GRADE KEYS',0,1);
					$pdf->SetFont('Times','',7);
					$pdf->SetY($pdf->GetY()+4);
					$pdf->SetX($pdf->GetX()+-10);
					$pdf->Cell(25,6,$col1,1,0,'L');
					$pdf->Cell(25,6,$col2,1,0,'L');
					$pdf->Cell(25,6,$col3,1,0,'L');
					$pdf->Cell(25,6,$col4,1,0,'L');
					$pdf->Cell(25,6,$col5,1,0,'L');
					$pdf->SetY($pdf->GetY()+4);
					$pdf->SetX($pdf->GetX()+-10);
					$pdf->SetFillColor(204,236,255);
					$pdf->Cell(25,6,$col6,1,0,'L',1);
					$pdf->Cell(25,6,$col7,1,0,'L',1);
					$pdf->Cell(25,6,$col8,1,0,'L',1);
					$pdf->Cell(25,6,$col9,1,0,'L',1);
					$pdf->Cell(25,6,$col10,1,0,'L',1);
					
					$pdf->SetY($pdf->GetY()+18);
					$pdf->SetFont('Times','B',12);
	  				$pdf->SetX($pdf->GetX()+-5);
    				$pdf->Cell(20,0,'Class Teacher'."'s". ' remark:',0,0,'L');
					$pdf->SetY($pdf->GetY()+2);
					
					$pdf->SetDrawColor(0,0,0);
	 				$pdf->SetFillColor(0,0,0);
	 				$pdf->SetLineWidth(0.4);
	  				$pdf->SetX($pdf->GetX()+ 40);
    				$pdf->Cell(40,0,'',1,1);
					
					$pdf->SetY($pdf->GetY()+-1);
					$pdf->SetX($pdf->GetX()+-105);
					$pdf->Cell(20,0,'Principal '."'s". ' remark:',0,0,'L');
					$pdf->SetY($pdf->GetY()+2);
					$pdf->SetX($pdf->GetX()+-100);
					$pdf->SetDrawColor(0,0,0);
	 				$pdf->SetFillColor(0,0,0);
	 				$pdf->SetLineWidth(0.4);
	  				$pdf->SetX($pdf->GetX()+ 28);
    				$pdf->Cell(40,0,'',1,1);
					
					$pdf->SetY($pdf->GetY()+-103);
					$pdf->SetX($pdf->GetX()+-109);
					$pdf->SetLineWidth(0.1);
					$pdf->SetFont('Times','B',11);
    				$pdf->Cell(40,5,'AFFECTIVE & PSYCHOMOTOR RATINGS',0,1);
					$pdf->SetFont('Times','',7);
					$pdf->SetX($pdf->GetX()+-100);
					
					$pdf->Cell(5,6,"5",1,0);
					$pdf->Cell(55,6,"Maintains an excellent degree of Observable trait ",1,0);
					
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-100);
					$pdf->SetFillColor(204,236,255);
					$pdf->Cell(5,6,"4",1,0,'L',1);
					$pdf->Cell(55,6,"Maintains a high level of observable traits",1,0,'L',1);			
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-100);
					$pdf->Cell(5,6,"3",1,0);
					$pdf->Cell(55,6,"Acceptable level of observable traits",1,0);
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-100);
					$pdf->SetFillColor(204,236,255);
					$pdf->Cell(5,6,"2",1,0,'L',1);
					$pdf->Cell(55,6,"Shows minimal regards for observable traits",1,0,'L',1);
					
					$pdf->SetY($pdf->GetY()+6);
					$pdf->SetX($pdf->GetX()+-100);;
					
					$pdf->Cell(5,6,"1",1,0,'L');
					$pdf->Cell(55,6,"Has no regard for observable traits",1,0,'L');
					
					$pdf->SetY($pdf->GetY()+6);
					
			
		}
		
					
	}
	else
	{
		
	}	
}
$pdf->Output();
?>